# Feature Addition / Bug Fixes

- [X] Add user's order count against each order
- [X] Add timestamp column agains Orders
- [ ] Add Feature to update payment gateway info in transactions
- [X] Update Prices
- [X] Update Product name
- [X] Add Single Product
