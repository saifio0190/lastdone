/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router        = require('express').Router();
const passport      = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const db            = require('../db');
const log4js        = require('log4js');
const logger        = log4js.getLogger('routes/login.route.js');
logger.level        = log4js.levels.INFO.levelStr;

router.get('/', (req, res) => {
	res.render('login', { layout: 'blank' });
});


// eslint-disable-next-line max-len
passport.use(new LocalStrategy({usernameField: 'email'}, async (email, password, done) => {
	const user = await db.admin.getUser(email);
	if (!user) {
		return done(null, false, {message: 'Unknown User'});
	}
	if (!user.active) {
		return done(null, false, { message: 'User Account pending activation' });
	}
	const isMatch = await db.admin.authenticateUser(password, user.password);
	logger.info(`ismatch is ${isMatch}`);
	if (isMatch) {
		return done(null, user);
	} else {
		return done(null, false, {message: 'Invalid password'});
	}
}));


passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
	const user = db.admin.getUserById(id);
	done(null, user);
});

router.post('/',
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/login',
		failureFlash: true,
	}),
	(req, res) => {
		res.redirect('/');
	});


module.exports = router;
