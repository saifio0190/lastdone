/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/', auth, async (req, res) => {


        

        const order = await db.orders.getliveorders();
      
       
        res.render('cart' , { order });
	
});



router.get('/livefeed', auth, async (req, res) => {


        

        const order = await db.orders.getlatestorders();
      

   
       
        res.json(order);
	
});


router.get('/dispatch', auth, async (req, res) => {


        

        const order = await db.orders.getdispatchorders();
        res.render('dispatch' , { orderpack:order.orderpac,orderdelivered:order.orderdel });
	
});
module.exports = router;
