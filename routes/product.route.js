/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/add', auth, async (req, res) => {
	const data = await db.product.getAllInfoForProductAddition();
	const payload = {
		group: data[0],
		category: data[1],
		company: data[2],
		brand: data[3],
		store: data[4],
		barcode: '900' + parseInt(Date.now() / 100).toString(),
	};
	res.render('single-product', payload);
});


router.get('/', auth, async (req, res) => {
	const storeId = req.query['storeId'];
	const stores = await db.store.getAllStores();
	const products = await db.store.getStoreCatalogue(storeId);
	res.render('catalogue', { stores, products });
});

router.get('/searchdata', auth, async (req, res) => {

	const products = await db.store.capturesearch();
	res.render('searchdata', { products });
});
router.get('/allproducts', auth, async (req, res) => {
	
	const stores = await db.store.getAllStores();
	const products = await db.store.getAllStoreCatalogue();
	
	res.render('allproducts', { stores, products });
});

module.exports = router;
