/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const log4js = require('log4js');
const logger = log4js.getLogger('routes/register.route.js');
logger.level = log4js.levels.INFO.levelStr;

router.get('/', (req, res) => {
	res.render('register', { layout: 'blank' });
});


router.post('/', async (req, res) => {
	const name = req.body.name;
	const email = req.body.email;
	const password = req.body.password;

	if (!Array.isArray(password)) {
		logger.info(`password in body was not an array ${password}`);
		res.redirect('/register');
		return;
	}

	if (password[0] !== password[1]) {
		logger.error(`passwords did not match ${password}`);
		res.redirect('/register');
		return;
	}

	const u = await db.admin.registerNewUser(name, email, password[0]);
	if (u) {
		res.redirect('/login');
	}
});

module.exports = router;
