/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');
const { driver } = require('../models');




router.get('/', auth, async (req, res) => {

	const storeId = req.query['storeId'];
	
	const drivers = await db.store.getDrivers(storeId);

	//const count = drivers.count();
	res.render('drivers', {  drivers:drivers.drivers ,count:drivers.driverscount,queuecount:drivers.queuecount,queue:drivers.queueorder});
});
router.post('/updateDriverStatus', auth, async (req, res) => {

	const id = req.body.id;
	const storeid = req.body.storeid;
	const status = req.body.status;
	
	var driver_name = req.body.driver_name;
	var driver_phone = req.body.driver_phone;

	var last_check_in = new Date(Date.now());
	var last_check_out = last_check_in;
	last_check_out = last_check_out.toString();
	var updateData = {
		storeid:storeid,
		check_in:status,
	}

	var insertData = {
    	storeid:storeid,
		check_in:status,
		driver_id:id,
		drivername:driver_name,
		phone_no:driver_phone,
	}
	if(status == true || status =='true'){
	updateData.last_check_in = last_check_out;
	insertData.last_check_in = last_check_out;
	}else{
		updateData.last_check_out = last_check_out;
		insertData.last_check_out = last_check_out;
	}
var updateDriverStatusRes = await db.store.updateDriverStatus(updateData,id);
var	insertDriverStatusRes = await db.store.insertDriverStatus(insertData);
	console.log(updateDriverStatusRes);
	
	res.send(updateDriverStatusRes);

});





router.get('/assigndriver',  async (req, res) => {


	
	const drivers = await db.store.assigndrivers(req.query.storeid);


	 //res.status(200).json(drivers);

	//res.json(JSON.stringify(drivers))l


	//const count = drivers.count();
	res.render('assign');
});




module.exports = router;
