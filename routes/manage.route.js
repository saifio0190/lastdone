/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/', auth, async (req, res) => {
	const manage = req.query['type'];
	if (!manage) {
		res.redirect('/');
		return;
	}
	if (manage === 'store') {
		const storeList = await db.store.getAllStoresWithShortLinks();
		res.render('store', { stores: storeList });
		return;
	}
	res.redirect('/');
});

module.exports = router;
