/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router    = require('express').Router();
const db        = require('../db');
const log4js    = require('log4js');
const logger    = log4js.getLogger('routes/api.route.js');
logger.level    = log4js.levels.INFO.levelStr;
const auth      = require('../middleware/auth');
var request = require("request");

const axios     = require('axios');

router.get('/status', auth, async (req, res) => {
	const orderId = req.query['orderId'];
	const status = req.query['status'];
	const person = req.query['personname'];
	const location = req.query['location'];
	const stid = req.query['storeid'];


	
	const paid = req.query['paid'] == 1 ? true : false;
	if (!orderId) {
		logger.info(`orderId was ${orderId}, returning...`);
		res.status(200).json({
			status: 'error',
			message: 'Invalid Order Id',
		});
		return;
	}
	if (
		// eslint-disable-next-line max-len
		!['RECEIVED', 'CONFIRMED', 'DISPATCHED', 'DELIVERED', 'CANCELLED'].includes(status)
	) {
		logger.info(`status was ${status}`);
		res.status(200).json({
			status: 'error',
			message: 'Invalid Status',
		});
		return;
	}

	const order = await db.orders.getOneOrders(orderId);

	if(status == 'DISPATCHED'){

		db.orders.createOrderQueue(orderId,stid);


		//const assign= await db.store.assigndrivers(stid);

		

		

		db.orders.updateOrderInventory(orderId, status, paid,location);

		console.log(order.gateway);

		if(order.gateway == "COD"){


			var options = { method: 'POST',
			url: 'https://api.cashfree.com/api/v1/order/info/link',
			headers: 
			 { 'postman-token': '394aae05-bb42-30ac-3855-c9d720d1055a',
			   'cache-control': 'no-cache',
			   'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
			formData: {'appId':"113938258568b005cd5050bd42839311","secretKey":"b9b4e5770b193eee6604183896f8ac964118ab2d","orderId":orderId} };
		  
		  request(options, function (error, response, body) {
			if (error) throw new Error(error);
		  
			console.log(body);
		  
			const obj = JSON.parse(body);
		  
			var payurl =  RemoveBaseUrl(obj.paymentLink);
		  
			var payurl1= escape(payurl);
		  
			console.log(payurl1);
		  
			console.log(order.user.phone);
		  
			
		  
		  
		  
			axios.get(`https://media.smsgupshup.com/GatewayAPI/rest?userid=2000195506&password=5C@U4UrB&&method=SENDMESSAGE&msg=Your+QUIKAF+order+${orderId}+has+been+dispatched+and+will+be+delivered+to+you+shortly.%0A%0ASince+you+have+selected+the+Payment+on+Delivery+%28POD%29+option%2C+you+can+pay+digitally+at+any+time+till+the+order+has+been+delivered+to+you.%0A%0AThis+will+help+maintain+social+distancing+and+also+help+us+to+keep+delivering+on+time%2C+every+time.&send_to=${order.user.phone}+&msg_type=TEXT&isTemplate=true&buttonUrlParam=${payurl1}&v=1.1&format=json&header=Order+Dispatched%21&footer=UPI%2C+Cards%2C+GPay%2C+PhonePe%2C+Paytm+%26+Net+Banking+accepted`)
				  .then(function (response) {
					// handle success
					console.log(response);
				  })
				  .catch(function (error) {
					// handle error
					console.log(error);
				  })
				  .then(function () {
					// always executed
				  });
		  });
		  


		}

	

		
		

		//return;
	}

	if(status == 'DELIVERED'){

		axios.get(`https://hapi.smsapi.org/SendSMS.aspx?UserName=Ekasta_live&password=Viva@5791&MobileNo=${order.user.phone}&SenderID=QUIKAF&CDMAHeader=QUIKAF&Message=Your%20order%20order%20${orderId}%20has%20been%20delivered.%20If%20delivered%20to%20security/bldg%20gate,%20its%20safety%20is%20your%20responsibility,%20please%20collect%20it%20ASAP%20-%20QUIKAF&dlt_templateid=1207161865411363772&coding=2`)
			.then(function (response) {
			  // handle success
			  console.log(response);
			})
			.catch(function (error) {
			  // handle error
			  console.log(error);
			})
			.then(function () {
			  // always executed
			});

		//return;
	}
	

	
	const _updated = await db.orders.updateOrderStatus(orderId, status, paid,person);
	if (!_updated) {
		res.status(200).json({
			status: 'error',
			message: 'Unable to update Status, please try later',
		});
		return;
	}
	res.status(200).json({
		status: 'success',
		message: `Updated status to ${status} for orderId ${orderId}`,
	});
});


router.get('/visible', auth, async (req, res) => {
	const storeId = req.query['storeId'];
	const barcode = req.query['barcode'];
	console.log('barcode',barcode)
	console.log('storeId',storeId)
	const visible = req.query['visible'] == 1 ? true : false;
     
	if (!storeId || !barcode) {
		// eslint-disable-next-line max-len
		logger.error(`storeid or barcode not set (${storeId}, ${barcode}, ${visible})`);
		res.status(200).json({
			status: 'error',
			message: 'Missing fields',
		});
		return;
	}

	// eslint-disable-next-line max-len
	const _updated = await db.store.updateProductVisibility(storeId, barcode, visible);

	if (!_updated) {
		// eslint-disable-next-line max-len
		logger.error(`updateProductVisibility returned false (${storeId}, ${barcode}, ${visible})`);
		res.status(200).json({
			status: 'error',
			message: 'Missing fields',
		});
		return;
	}
	res.status(200).json({
		status: 'success',
		message: `updated visibility to ${visible} for ${barcode}`,
	});
});


router.post('/shortlink', async (req, res) => {
	const shortlink = req.body.shortlink;
	const key = req.body.key;
	if (key !== 'FmHzs9z3+zHad715u29dgQ==') {
		logger.error(`key did not match: ${key}`);
		res.sendStatus(400);
		return;
	}
	const _inserted = await db.shortlink.insertShortlink(shortlink);
	if (!_inserted) {
		logger.error(`inserted was false`);
		res.sendStatus(500);
		return;
	}
	res.sendStatus(200);
});


router.get('/visibility', auth, async (req, res) => {
	const storeId = req.query['storeId'];
	const visible = req.query['visible'] == 1 ? true : false;
	if (!storeId) {
		logger.error(`storeid not set (${storeId}, ${visible})`);
		res.status(200).json({
			status: 'error',
			message: 'Missing fields',
		});
		return;
	}
	await db.store.toggleVisibiliyForStoreProducts(store, visible);
	res.sendStatus(200);
});



router.get('/updatedeliverystatus',  async (req, res) => {
	const storeId = req.query['orderid'];

	await db.store.updatedeliverystatus(storeId);
	res.sendStatus(200);
});


router.get('/backtohome',  async (req, res) => {
	const storeId = req.query['driverid'];

	await db.store.updatebacktohomestatus(storeId);
	res.sendStatus(200);
});


router.get('/catalogue', auth, async (req, res) => {
	const barcode = req.query['barcode'];
	const storeId = req.query['storeId'];

	const product = await db.store.getProductFromCatalogue(barcode, storeId);

	if (!product) {
		res.sendStatus(500);
		return;
	}

	res.json(product);
});


router.post('/catalogue', auth, async (req, res) => {
	const isUpdated =  await db.store.updateProductInCatalogue(req.body);
	if (isUpdated) {
		res.sendStatus(200);
		return;
	}
	res.sendStatus(500);
});


router.get('/barcode-check', auth, async (req, res) => {
	const barcode = req.query['barcode'];
	if (!barcode) {
		res.redirect('/');
		return;
	}
	const isAvailable = await db.product.barcodeCheck(barcode);
	if (isAvailable) {
		res.sendStatus(200);
		return;
	}
	res.sendStatus(500);
});

router.post('/product-add', auth, async (req, res) => {
	const payload = req.body;
	const created = await db.product.addProductsToCatalogue(payload);
	if (created) {
		res.sendStatus(200);
		return;
	}
	res.sendStatus(500);
});




function RemoveBaseUrl(url) {
    /*
     * Replace base URL in given string, if it exists, and return the result.
     *
     * e.g. "http://localhost:8000/api/v1/blah/" becomes "/api/v1/blah/"
     *      "/api/v1/blah/" stays "/api/v1/blah/"
     */
    var baseUrlPattern = /^https?:\/\/[a-z\:0-9.]+/;
    var result = "";
    
    var match = baseUrlPattern.exec(url);
    if (match != null) {
        result = match[0];
    }
    
    if (result.length > 0) {
        url = url.replace(result, "");
    }
    
    return url.substring(1);
}

module.exports = router;
