/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');

const cron = require('node-cron');
cron.schedule('* * * * *', function() {
	console.log('running a task every minute');
	console.log(process.env.name ,"process");
  });

  console.log(process.env.name ,"process");
router.get('/', auth, async (req, res) => {
	let storeId = req.query['storeId'];
	let status = req.query['status'];
	var old = 'no';
	old = req.query['old'];
	console.log('')
	if (storeId === 'all') {
		storeId = null;
	}
	if (
		// eslint-disable-next-line max-len
		!['RECEIVED', 'CONFIRMED', 'DISPATCHED', 'DELIVERED', 'CANCELLED'].includes(status)
	) {
		status = null;
	}
	const stores = await db.store.getAllStores();
	const orders = await db.orders.getStoreOrders(storeId, status,old);
	res.render('stores', {
		stores,
		orders: orders.orders,
		additional: orders.additional,
		orderdiv:orders.orderdiv,
	});
});

module.exports = router;
