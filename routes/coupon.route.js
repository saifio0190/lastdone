/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const fs        = require('fs');
const auth   = require('../middleware/auth');
var request   = require('request');




function writePromoData(lineData){
  var result = {status:0,data:'failed'};
  
  var promo = new Promise((resolve, reject) => {
      
      const stream = fs.createWriteStream('./promotion_data.csv',{flags:'a'});
  stream.write(lineData +"\r\n ",function(){
      result = {status:1,data:'success'};
      
  return resolve(result);
  })
  });
  return promo;
  }
router.get('/download',async function(req, res){
    
  var from = `${req.query.fromDate} ${req.query.fromTime}:56.552+05:30`;
  var to = `${req.query.toDate} ${req.query.toTime}:56.552+05:30`;
  //var to = `2021-01-29 21:53:56.552+05:30`;
  const file = './promotion_data.csv';
  fs.unlink(file, function(err) {
    if(err && err.code == 'ENOENT') {
        // file doens't exist
        console.info("File doesn't exist, won't remove it.");
    } else if (err) {
        // other errors, e.g. maybe we don't have enough permission
        console.error("Error occurred while trying to remove file");
    } else {
        console.info(`removed`);
    }
});
  const allPromos = await db.store.getAllStoreCatalogue();
   var allPromosLenth = allPromos.length;
   var counter = 0;
 
  allPromos.forEach(async (item,index) =>{
    
      var data = item.barcode+","+item.name+","+item.displayName;
    
    await writePromoData(data);
    counter ++;
    if(counter == allPromosLenth -1)
    res.download(file); 
  
  })

  });

router.get('/', async (req, res) => {
 
    const data = await db.coupon.getAllCoupon();
   
	const payload = {
		type: data[0],
		coupons: data[1],
	};
   res.render('allcoupon',payload);
  // res.send('allPromo');
});

router.get('/edit', async (req, res) => {
 
  const type = await db.coupon.getcouponType();
  var id = req.query.id;
  const data = await db.coupon.getcoupon(id);
 
const payload = {
  type: type,
  data:data,
  
};
 res.render('editcoupon',payload);
// res.send('allPromo');
});
router.get('/getCoupon', async (req, res) => {
var result = {status:0, data:'failed'};
  var id = req.query.id;
  const data = await db.coupon.getcoupon(id);
 if(data){
result.status =1;
result.data =data;
 }

res.send(result);
});
router.get('/generate', async (req, res) => {
    const type = await db.coupon.getcouponType();
    
  const payload = {
    type: type,
    
  };
   res.render('editcoupon',payload);
  });
router.get('/addnew', async (req, res) => {
 
  const data = await db.product.getAllInfoForProductAddition();
 
const payload = {
  group: data[0],
  category: data[1],
  company: data[2],
  brand: data[3],
   store: data[4],
    location:data[5],
    type:data[6],
  barcode: '900' + parseInt(Date.now() / 100).toString(),
};
 res.render('add_plain_new',payload);
// res.send('allPromo');
});

router.get('/inventory-detail-by-barcode', async (req, res) => {

  const data = await db.inventory.getInventoryByBarcode(req.query);
  console.log('data',data);
  res.send(data);
});

router.get('/product-detail-by-barcode', async (req, res) => {

    const data = await db.inventory.getProductByBarcode(req.query);
    res.send(data);
});
router.post('/coupon-update', async (req, res) => {
 
  var updateData = req.body.updateData;
  var id = req.body.id;
  const dataUpdated= await db.coupon.updateCouponById(updateData,id);
  console.log('dataUpdated',dataUpdated);
  res.send(dataUpdated);
});
router.post('/coupon-add', async (req, res) => {

  var insertData = req.body.insertData;
  var name  = req.body.name;
  var dataStore = {status:0,data:'failed'};

 var dataStore1 = await db.coupon.addCoupon(insertData, name);
 console.log('dataStore',dataStore);
 if(dataStore1 == true || dataStore1 == 'true'){
    console.log('dataStore here',dataStore);
    dataStore.data ="Existing Coupon Found";
    dataStore.status = 0;
 }
 if(dataStore1.status == 1){
    dataStore.status = 1;
 }

   
 
  res.send(dataStore);
});




module.exports = router;
