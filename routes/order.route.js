/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/timeline', auth, async (req, res) => {
	const orders = await db.orders.getOrderByOrder();
	res.render('timeline', { orders: orders });
});

router.get('/openorder', async (req, res) => {
 
	const data = await db.product.getAllInfoForProductAddition();
   
  const payload = {
	group: data[0],
	category: data[1],
	company: data[2],
	brand: data[3],
	 store: data[4],
	  location:data[5],
	  type:data[6],
	barcode: '900' + parseInt(Date.now() / 100).toString(),
  };
   res.render('openorder',payload);
});

router.get('/todaysOrder', auth, async (req, res) => {
	const stores = await db.orders.todaysOrder();

   res.send(stores);
});
router.get('/monthsOrder', auth, async (req, res) => {
	const stores = await db.orders.monthsOrder();

   res.send(stores);
});
router.get('/dailyOrder', auth, async (req, res) => {
	const stores = await db.orders.dailyOrder();

   res.send(stores);
});




router.get('/', auth, async (req, res) => {
	const storeId = req.query['storeId'];
	const status = req.query['status'];
	var old = 'no';
	 old = req.query['old'];
	 console.log('old',old);

	
	const paidStatus = req.query['paid'];
	console.log(paidStatus);
	const stores = await db.store.getAllStoresALl();
	const packingpersons = await db.orders.getallpackingpersons(storeId);

	var personarr = []

	packingpersons.forEach( async element => {
		

		console.log(element);

		personarr.push({'id':element.id,'name':element.name});
		
		
						
						})

	console.log(personarr);

	
	const orders = await db.orders.getAllOrders(storeId, status, paidStatus,old);
	res.render('orders', { stores, orders,personarr });
});
router.get('/unpaid', auth, async (req, res) => {
	const storeId = req.query['storeId'];
	const status = req.query['status'];
	var old = 'no';
	 old = req.query['old'];
	 console.log('old',old);

	
	const paidStatus = req.query['paid'];
	const stores = await db.store.getAllStoresALl();
	const packingpersons = await db.orders.getallpackingpersons(storeId);

	var personarr = []

	packingpersons.forEach( async element => {

		personarr.push({'id':element.id,'name':element.name});
		
		
						
						})

	

	
	const ordersss =  await db.orders.getAllUnpaidOrders(storeId, status, paidStatus,old);

	res.render('unpaid', { stores, ordersss,personarr });
});


router.get('/orderitems', auth, async (req, res) => {

	const ordersss =  await db.orders.getAllOrderitems();

	res.render('orderitems', { ordersss });
});
module.exports = router;
