/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');
const log4js = require('log4js');
const logger = log4js.getLogger('route/additional.route.js');
logger.level = log4js.levels.INFO.levelStr;


router.get('/edit', auth, async (req, res) => {
	const id = req.query['id'];
	if (!id) {
		res.redirect('/additionals');
		return;
	}
	const additional = await db.additional.getAdditional(id);
	res.render('additional', { additional });
});


router.get('/', auth, async (req, res) => {
	const additionals = await db.additional.getAllAdditionals();
	res.render('additionals', { additionals });
});

module.exports = router;

