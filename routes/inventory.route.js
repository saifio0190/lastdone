/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const fs        = require('fs');
const auth   = require('../middleware/auth');
var request   = require('request');




function writePromoData(lineData){
  var result = {status:0,data:'failed'};
  
  var promo = new Promise((resolve, reject) => {
      
      const stream = fs.createWriteStream('./promotion_data.csv',{flags:'a'});
  stream.write(lineData +"\r\n ",function(){
      result = {status:1,data:'success'};
      
  return resolve(result);
  })
  });
  return promo;
  }
router.get('/download',async function(req, res){
    
  var from = `${req.query.fromDate} ${req.query.fromTime}:56.552+05:30`;
  var to = `${req.query.toDate} ${req.query.toTime}:56.552+05:30`;
  //var to = `2021-01-29 21:53:56.552+05:30`;
  const file = './promotion_data.csv';
  fs.unlink(file, function(err) {
    if(err && err.code == 'ENOENT') {
        // file doens't exist
        console.info("File doesn't exist, won't remove it.");
    } else if (err) {
        // other errors, e.g. maybe we don't have enough permission
        console.error("Error occurred while trying to remove file");
    } else {
        console.info(`removed`);
    }
});
  const allPromos = await db.store.getAllStoreCatalogue();
   var allPromosLenth = allPromos.length;
   var counter = 0;
 
  allPromos.forEach(async (item,index) =>{
    
      var data = item.barcode+","+item.name+","+item.displayName;
    
    await writePromoData(data);
    counter ++;
    if(counter == allPromosLenth -1)
    res.download(file); 
  
  })

  });

router.get('/', async (req, res) => {
 
    const data = await db.product.getAllInfoForProductAddition();
   
	const payload = {
		group: data[0],
		category: data[1],
		company: data[2],
		brand: data[3],
     store: data[4],
      location:data[5],
      type:data[6],
      storage:data[7],
		barcode: '900' + parseInt(Date.now() / 100).toString(),
	};
   res.render('update_add_inventory',payload);
  // res.send('allPromo');
});

router.get('/allocate', async (req, res) => {
 
  const data = await db.product.getAllInfoForProductAddition();
 
const payload = {
  group: data[0],
  category: data[1],
  company: data[2],
  brand: data[3],
   store: data[4],
    location:data[5],
    type:data[6],
  barcode: '900' + parseInt(Date.now() / 100).toString(),
};
 res.render('allocate',payload);
// res.send('allPromo');
});
router.get('/addnew', async (req, res) => {
 
  const data = await db.product.getAllInfoForProductAddition();
 
const payload = {
  group: data[0],
  category: data[1],
  company: data[2],
  brand: data[3],
   store: data[4],
    location:data[5],
    type:data[6],
  barcode: '900' + parseInt(Date.now() / 100).toString(),
};
 res.render('add_plain_new',payload);
// res.send('allPromo');
});

router.get('/inventory-detail-by-barcode', async (req, res) => {

  const data = await db.inventory.getInventoryByBarcode(req.query);
  console.log('data',data);
  res.send(data);
});

router.get('/inventory-occupancy', async (req, res) => {
 var storeId = req.query.storeId;
  const data = await db.inventory.getstoragecapacity(storeId);
  
  //return {refrigeratoravailiablespace,refrigeratoroccupiedspace,fridgeavailiablespace,fridgeoccupiedspace,regularavailiablespace,regularoccupiedspace};

   
   res.render('inventory-occupancy',{refrigeratoravailiablespace:data.refrigeratoravailiablespace,refrigeratoroccupiedspace:data.refrigeratoroccupiedspace,fridgeavailiablespace:data.fridgeavailiablespace,fridgeoccupiedspace:data.fridgeoccupiedspace,regularavailiablespace:data.regularavailiablespace,regularoccupiedspace:data.regularoccupiedspace ,products:data.products});
});

router.get('/inventory-occupancy-brand', async (req, res) => {
 
  var storeId = req.query.storeId;
  var brand = req.query.brand;
	const data = await db.inventory.getstoragecapacityBrand(storeId,brand);
  res.render('inventory-occupancy-brand',{refrigeratoravailiablespace:data.refrigeratoravailiablespace,refrigeratoroccupiedspace:data.refrigeratoroccupiedspace,fridgeavailiablespace:data.fridgeavailiablespace,fridgeoccupiedspace:data.fridgeoccupiedspace,regularavailiablespace:data.regularavailiablespace,regularoccupiedspace:data.regularoccupiedspace ,products:data.products,brands:data.brands});
  //  res.render('inventory-occupancy-brand',payload);
});

router.get('/product-detail-by-barcode', async (req, res) => {

    const data = await db.inventory.getProductByBarcode(req.query);
    res.send(data);
});
router.post('/product-update', async (req, res) => {
 
  var updateDataBarcode = req.body.updateDataBarcode;
  var updateDataStore = req.body.updateDataStore;
  var barcodeData = req.body.barcodeData;
  console.log('barcodeData',barcodeData);
  var barcode = req.body.barcode;
  var whlocation = req.body.whlocation;
  var storageUpdateFlag = req.body.storageUpdateFlag;
  console.log('storageUpdateFlag',storageUpdateFlag);
  var storageId = req.body.storageId;
  var updateStorage = req.body.updateStorage;

  const dataBarcode = await db.inventory.updateBarcodeProductByBarcode(updateDataBarcode,barcode);
  const dataStore = await db.inventory.updateStoreProductByBarcode(updateDataStore,barcode,whlocation,barcodeData);
  if(storageUpdateFlag > 0){
    const dataStorage = await db.inventory.updateStorage(storageId,updateStorage);
  }

 
  res.send(dataStore);
});
router.post('/product-add', async (req, res) => {

  insertDataStore  = req.body.insertDataStore;
  insertDataBarcode  = req.body.insertDataBarcode;
  var storageUpdateFlag = req.body.storageUpdateFlag;
  var storageId = req.body.storageId;
  var updateStorage = req.body.updateStorage;
  console.log('insertDataBarcode',insertDataBarcode);
  console.log('insertDataStore',insertDataStore);
  var whlocation = req.body.whlocation;
  var dataStore = {status:0,data:'failed'};
 const barcodeCheck = await db.inventory.barcodeCheck(insertDataStore.barcode,whlocation);
 console.log('barcodeCheck',barcodeCheck);
 if(barcodeCheck){
  dataStore = await db.inventory.addStoreProductByBarcode(insertDataStore);
    const dataBarcode = await db.inventory.addBarcodeProductByBarcode(insertDataBarcode);
    if(storageUpdateFlag > 0){
      const dataStorage = await db.inventory.updateStorage(storageId,updateStorage);
    }
   
 }else{
  dataStore.data = " product found with this barcode";
  
 }
 
  res.send(dataStore);
});




module.exports = router;
