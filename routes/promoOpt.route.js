/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const fs        = require('fs');
const auth   = require('../middleware/auth');
var request   = require('request');



function writePromoData(lineData){
  var result = {status:0,data:'failed'};
  
  var promo = new Promise((resolve, reject) => {
      
      const stream = fs.createWriteStream('./promotion_data.csv',{flags:'a'});
  stream.write(lineData +"\r\n ",function(){
      result = {status:1,data:'success'};
      
  return resolve(result);
  })
  });
  return promo;
  }
router.get('/download',async function(req, res){
    
  var from = `${req.query.fromDate} ${req.query.fromTime}:56.552+05:30`;
  var to = `${req.query.toDate} ${req.query.toTime}:56.552+05:30`;
  //var to = `2021-01-29 21:53:56.552+05:30`;
  const file = './promotion_data.csv';
  fs.unlink(file, function(err) {
    if(err && err.code == 'ENOENT') {
        // file doens't exist
        console.info("File doesn't exist, won't remove it.");
    } else if (err) {
        // other errors, e.g. maybe we don't have enough permission
        console.error("Error occurred while trying to remove file");
    } else {
        console.info(`removed`);
    }
});
  const allPromos = await db.promoOpt.getAllPromos(from,to);
   var allPromosLenth = allPromos.length;
   var counter = 0;
 
  allPromos.forEach(async (item,index) =>{
    
      var data = item.mobile+","+item.promotor+","+item.createdAt;
    
    await writePromoData(data);
    counter ++;
    if(counter == allPromosLenth -1)
    res.download(file); 
  
  })

  });

router.get('/', auth, async (req, res) => {
  // const file = './promotion_data.csv';
  // fs.unlinkSync(file);
  const allPromo = await db.promoOpt.getAllPromoOpts();
  // allPromo.forEach(async(item) =>{
  //   var data = item.mobile+","+item.opt_in_id+","+item.greet+","+item.response_greet_json;
  //   console.log(data);
    
  //   await writePromoData(data);
  // })
  // console.log()
  
   res.render('promoOpt', { allPromo });
  // res.send('allPromo');
});
router.post('/sendGreet', auth, async (req, res) => {
    var  updateData = {};
     console.log(req.body);
    updateData.opt_in_id = req.body.opt_in_id;
    updateData.greet = 0;
    updateData.mobile = req.body.mobile;
    var greetSend2 = await greetSend(req.body.mobile);
    if(greetSend2.data.response.status == 'success')
    updateData.greet = 1;

delete updateData.mobile; 
delete updateData["mobile"];

    updateData.response_greet_json = JSON.stringify(greetSend2);
	const sendGreet = await db.promoOpt.updatePromoGreet(updateData);
    
    // console.log(sendGreet);
    // console.log(greetSend2);
   res.send(sendGreet);
	
});



function greetSend(mobile1){
    var mobile = mobile1;
    if(mobile.length < 11)
    mobile = "91"+mobile;
    var mesag ="Dear%20Customer%0A%0AThank%20you%20for%20creating%20an%20account%20on%20Near.Store.%20An%20amount%20of%20INR%20100%2F-%20has%20been%20credited%20to%20your%20account.%20It%20will%20be%20automatically%20applied%20when%20you%20check%20out%20for%20your%20next%20order.%20%0A%0AThanks%0ATeam%20Near.Store";
    var a = "https://media.smsgupshup.com/GatewayAPI/rest?method=SendMessage&format=json&userid=2000195506&password=5C@U4UrB&send_to="+mobile+"&v=1.1&auth_scheme=plain&msg_type=HSM&isTemplate=true&msg="+mesag;
    var urldat = a;
        var result = {status:0,data:'failed'};
          var options2 = { 
          url: urldat, 
          method: 'GET'
          };
          try{
            var resuldata = new Promise((resolve, reject) => {
              request.get(options2, function(error, response, body) {
                if(!error){ 
                if(response.statusCode == 200 || response.statusCode == 201 ){
                result.data = JSON.parse(body);
                result.status = 1;
                resolve(result);
                console.log('checjbody',body);
                }else{
                result.status = 0;
                result.data = body;
                resolve(result);
                }
                }else{
                  console.log(error);
                  result.status = 0;  
                  resolve(result);      
                  }
                });
          
            })
          
            return resuldata;
            
          }catch(e){
            console.log('err')
            return e;
          } 
      }


module.exports = router;
