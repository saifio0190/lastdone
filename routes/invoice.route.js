/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/', auth, async (req, res) => {
	const orderId = req.query['orderId'];
	const print = req.query['print'];
	if (!orderId) {
		res.redirect('/order');
		return;
	}
	const order = await db.orders.getOneOrders(orderId);
	if (print) {
		res.render('print', { order: order, layout: 'print' });
	} else {
		res.render('item1', { order });
	}
});



router.get('/inv', auth, async (req, res) => {
	const orderId = req.query['orderId'];
	const print = req.query['print'];
	if (!orderId) {
		res.redirect('/order');
		return;
	}
	const order = await db.orders.getOneOrders(orderId);

	console.log(order);
	if (print) {
		res.render('print', { order: order, layout: 'print' });
	} else {
		res.render('inv', { order });
	}
});



router.get('/inv2',  async (req, res) => {
	const orderId = req.query['orderId'];
	const print = req.query['print'];
	if (!orderId) {
		res.redirect('/order');
		return;
	}
	const order = await db.orders.getOneOrders(orderId);

	console.log(order);
	if (print) {
		res.render('print', { order: order, layout: 'print' });
	} else {
		res.render('invpub', { order ,layout:false });
	}
});


router.get('/inv3',  async (req, res) => {
	const orderId = req.query['orderId'];
	const print = req.query['print'];
	// if (!orderId) {
	// 	res.redirect('/order');
	// 	return;
	// }
	const order = await db.orders.getOneOrdersCsv(orderId);

	order.orderitems.forEach(function(entry) {
		var invid = 'QF/21/5/'+order.id;

		var date = datefor(order.createdAt);

		var cgst  = parseFloat(entry.cgst * 100 ).toFixed(1);
		var sgst  = parseFloat(entry.sgst * 100 ).toFixed(1);
		var cess  = parseFloat(entry.cess * 100 ).toFixed(1);
		
		console.log(order.order_id+","+invid+","+cgst+","+entry.cgstamt+","+sgst+","+entry.sgstamt+","+cess+","+entry.cessamt+","+entry.pretaxvalue+","+entry.frp);
		
	
	});


	// try {

	// 	console.log(order.order_id+','+order.user.email+','+order.bill_amount+','+order.mrpv+','+order.createdAt);

		
	// } catch (error) {
		
	// }

	res.send("hello");

});


router.get('/inv1/',  async (req, res) => {
	const orderId = req.query['orderId'];
	const print = req.query['print'];
	if (!orderId) {
		res.redirect('/order');
		return;
	}
	const order = await db.orders.getOneOrders(orderId);

	//console.log(order.orderitems);
	if (print) {
		res.render('print', { order: order, layout: 'print' });
	} else {
		res.render('invpub', { order , layout:false });
	}
});



router.get('/deleteproduct', auth, async (req, res) => {
	const pid = req.query['id'];

	const qty = req.query['qty'];

	const billamt = req.query['bill'];

	const itembillamt = req.query['itembill'];
	
	console.log(pid);
	const order = await db.orders.delteOrders(pid,qty,billamt,itembillamt,req.query['oid']);

	

	res.redirect('/invoice?orderId='+req.query['oid']);
	
});



function datefor (date) {
	var todayTime = new Date(date);
	var month = todayTime . getMonth()+1;
	var day = todayTime . getDate();
	var year = todayTime . getFullYear();
	return month + "/" + day + "/" + year;
	}

module.exports = router;
