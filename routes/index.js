/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router            = require('express').Router();
const loginRoutes       = require('./login.route');
const registerRoutes    = require('./register.route');
const appRoutes         = require('./app.route');
const orderRoutes       = require('./order.route');
const invoiceRoutes     = require('./invoice.route');
const storeRoutes       = require('./store.route');
const apiRoutes         = require('./api.route');
const productRoutes     = require('./product.route');
const manageRoutes      = require('./manage.route');
const additionalRoutes  = require('./additional.route');
const promoOpt  = require('./promoOpt.route');
const logistic  = require('./logistic.route');
const Inventory  = require('./inventory.route');
const deliverydriver  = require('./deliverydriver.route');
const coupon  = require('./coupon.route');
const whatsapp  = require('./whatsapp.route');

const cartRoutes  = require('./cart.route');




const auth   = require('../middleware/auth');
const { driver } = require('../models');


router.use('/api', apiRoutes);
router.use('/login', loginRoutes);
router.use('/order', orderRoutes);
router.use('/store', storeRoutes);

router.use('/cart', cartRoutes);
router.use('/invoice', invoiceRoutes);
router.use('/product', productRoutes);
router.use('/manage', manageRoutes);
router.use('/additional', additionalRoutes);
router.use('/promoOpt', promoOpt);
router.use('/logistic', logistic);
router.use('/whatsapp', whatsapp);
router.use('/drivers', deliverydriver);
router.use('/inventory', Inventory);
router.use('/coupon', coupon);

router.use('/register', registerRoutes);
router.use('/', appRoutes);

router.get('/logout', auth, (req, res) => {
	req.logout();
	res.redirect('/login');
});

module.exports = router;
