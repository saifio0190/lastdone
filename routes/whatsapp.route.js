/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const fs        = require('fs');
const auth   = require('../middleware/auth');
var request   = require('request');
var headerData = {'Accept-Language' : 'en_US',"content-type" : "application/json"};



function writePromoData(lineData){
  var result = {status:0,data:'failed'};
  
  var promo = new Promise((resolve, reject) => {
      
      const stream = fs.createWriteStream('./whatsapp_data.csv',{flags:'a'});
  stream.write(lineData +"\r\n ",function(){
      result = {status:1,data:'success'};
      
  return resolve(result);
  })
  });
  return promo;
  }
router.get('/download',async function(req, res){
    
  var from = `${req.query.fromDate} ${req.query.fromTime}:56.552+05:30`;
  var to = `${req.query.toDate} ${req.query.toTime}:56.552+05:30`;
  //var to = `2021-01-29 21:53:56.552+05:30`;
  const file = './delivery_data.csv';
  fs.unlink(file, function(err) {
    if(err && err.code == 'ENOENT') {
        // file doens't exist
        console.info("File doesn't exist, won't remove it.");
    } else if (err) {
        // other errors, e.g. maybe we don't have enough permission
        console.error("Error occurred while trying to remove file");
    } else {
        console.info(`removed`);
    }
});
  //fs.unlinkSync(file);
  const allLogistics = await db.logistic.getDownload(from,to);
   var allLogisticsLenth = allLogistics.length;
   var counter = 0;
 
   allLogistics.forEach(async (item,index) =>{
    
    var data = item.order_id+","+item.user_id+","+item.task_id+","+item.store_id+","+item.state+","+item.source+","+item.estimated_price+","+item.used_API+","+item.runner_phone_number+","+item.track_url+","+item.start_point+","+item.end_point+","+item.createdAt;
    
    await writePromoData(data);
    counter ++;
    if(counter == allLogisticsLenth -1)
    res.download(file); 
  
  })

  });
  router.get('/downloadById',async function(req, res){
    
    var order_id_Search = `${req.query.order_id_Search}`;
    const file = './whatsapp_data.csv';
    fs.unlink(file, function(err) {
        if(err && err.code == 'ENOENT') {
            // file doens't exist
            console.info("File doesn't exist, won't remove it.");
        } else if (err) {
            // other errors, e.g. maybe we don't have enough permission
            console.error("Error occurred while trying to remove file");
        } else {
            console.info(`removed`);
        }
    });
    const allLogistics = await db.whatsapp.downloadById(order_id_Search);
    console.log('allLogistics',allLogistics);
     var allLogisticsLenth = allLogistics.length;
     var counter = 0;
     console.log('allLogisticsLenth',allLogisticsLenth);
     allLogistics.forEach(async (item,index) =>{
      
      var data = item.mobile+","+item.name+","+item.count+","+item.created_on+","+item.message;
      
      await writePromoData(data);
      counter ++;
      if(allLogisticsLenth > 1){
      if(counter == allLogisticsLenth -1)
      res.download(file); 
      }else{
        res.download(file);  
      }
     
    
    })
  
    });

router.get('/', auth, async (req, res) => {

  const getWhatsapp = await db.whatsapp.getAllWhatsapp(); 
//   console.log(alllogistics);
//   res.send(alllogistics);
   res.render('whatsapp', { getWhatsapp });
 
});
router.post('/cancelDelivery', auth, async (req, res) => {
  var cancelApiData={status:0,data:'something went wromg'};
    var data = {
        order_id:req.body.order_id,
        cancellation_reason:'cencelled user',
        reason:'cencelled user',
        user:'Seller',
        source:req.body.source
    }
     if(!!req.body.source && !! req.body.order_id){
    cancelApiData = await cancelDelivery(data,req.body.source)
     }else{
         cancelApiData.status=0;
         cancelApiData.data="insufficient data provided";
     }
     res.send(cancelApiData);
	
});
router.get('/trackStatus', auth, async (req, res) => {
  var cancelApiData={status:0,data:'something went wromg'};
    var data = {
        order_id:req.query.order_id,
        source:req.query.source
    }
    console.log(data);
     if(!!req.query.source && !! req.query.order_id){
    cancelApiData = await trackStatus(req.query.order_id,req.query.source)
     }else{
         cancelApiData.status=0;
         cancelApiData.data="insufficient data provided";
     }
     res.send(cancelApiData);
	
});

function cancelDelivery(body_data,source){
  console.log('body_data',body_data);
    var result = {status:0,data:'failed'};
  //  var urlVal = 'http://localhost:8091/shadowfaxAPI/cancelOrder';
    var urlVal = 'https://staging.near.store/wh/shadowfaxAPI/cancelOrder';
    var methodCall = 'PUT';
    var methodCall1 = 'put';
    if(source =='dunzo'){
   var urlVal = 'https://staging.near.store/wh/dunzoAPI/cancelTask';
    // var urlVal = 'http://localhost:8091/dunzoAPI/cancelTask';
     methodCall = 'POST';
}

 
    var options2 = {
    url: urlVal,
    headers: headerData,  
    body: JSON.stringify(body_data),

    method: methodCall
    };
  
    try{
      var resuldata = new Promise((resolve, reject) => {
              if(source =='dunzo'){
            request.post(options2, function(error, response, body) {
              if(!error){ 
              if(response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 204 ){
              result.data = body;
              result.status = 1;
              resolve(result);
              }else{
              result.status = 0;
              result.data = body;
              resolve(result);
              }
              }else{
                result.status = 0;  
                resolve(result);      
                }
              }); 
          }else{
            request.put(options2, function(error, response, body) {
              if(!error){ 
              if(response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 204 ){
              result.data = body;
              result.status = 1;
              resolve(result);
              }else{
              result.status = 0;
              result.data = body;
              resolve(result);
              }
              }else{
                result.status = 0;  
                resolve(result);      
                }
              }); 
          }          
      })
      return resuldata;  
    }catch(e){
      return e;
    } 
}
function trackStatus(order_id,source){

  var result = {status:0,data:'failed'};
  // var urlVal = 'http://localhost:8091/shadowfaxAPI/getStatus?order_id='+order_id;
   var urlVal = 'https://staging.near.store/wh/shadowfaxAPI/getStatus?order_id='+order_id;
  var methodCall = 'GET';
  if(source =='dunzo'){
    // var urlVal = 'http://localhost:8091/dunzoAPI/trackTask?order_id='+order_id;
  var urlVal = 'https://staging.near.store/wh/dunzoAPI/trackTask?order_id='+order_id;
   methodCall = 'GET';
}


  var options2 = {
  url: urlVal,
  headers: headerData, 
  method: methodCall
  };

  try{
    var resuldata = new Promise((resolve, reject) => {
      request.get(options2, function(error, response, body) {
        if(!error){ 
        if(response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 204 ){
        result.data = body;
        result.status = 1;
        resolve(result);
        }else{
        result.status = 0;
        result.data = body;
        resolve(result);
        }
        }else{
          result.status = 0;  
          resolve(result);      
          }
        }); 
    })
    return resuldata;  
  }catch(e){
    return e;
  } 
}

module.exports = router;
