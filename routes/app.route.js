/* eslint-disable no-multi-spaces */
// eslint-disable-next-line new-cap
const router = require('express').Router();
const db     = require('../db');
const auth   = require('../middleware/auth');


router.get('/', auth, async (req, res) => {
	const data = await db.orders.dashboardData();
	res.render('home', data);
});

module.exports = router;
