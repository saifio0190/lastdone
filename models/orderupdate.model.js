'use strict';

module.exports = (sequelize, DataTypes) => {
	const orderupdate = sequelize.define('orderupdate', {
		action: DataTypes.STRING,
		order_id: DataTypes.STRING,
		barcodemasterId: DataTypes.STRING,
        prev_data: DataTypes.STRING,
        cur_data: DataTypes.STRING,
	}, {});
	orderupdate.associate = function(models) {
		// Create associations here
	};
	return orderupdate;
};
