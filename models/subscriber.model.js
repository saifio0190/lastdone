'use strict';

module.exports = (sequelize, DataTypes) => {
	const subs = sequelize.define('subscriber', {
		email: DataTypes.STRING,
	}, {});
	subs.associate = function(models) {
		// Create associations here
	};
	return subs;
};
