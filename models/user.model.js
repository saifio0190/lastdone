'use strict';

module.exports = (sequelize, DataTypes) => {
	const user = sequelize.define('user', {
		uid: DataTypes.STRING,
		name: DataTypes.STRING,
		email: DataTypes.STRING,
		phone: DataTypes.STRING,
		gender: DataTypes.STRING,
		address: DataTypes.TEXT,
		dob: DataTypes.STRING,
		city: DataTypes.STRING,
		country: DataTypes.STRING,
		loc: DataTypes.JSON,
		discountavail:DataTypes.STRING,
	}, {});
	user.associate = function(models) {
		// Create associations here
		user.hasMany(models.order);
	};
	return user;
};
