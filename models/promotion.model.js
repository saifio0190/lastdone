module.exports = (sequelize, Sequelize) => {
    const Promotion = sequelize.define("promotion", {
      fullname: {
        type: Sequelize.STRING
      },
      uid: {
        type: Sequelize.STRING
      },
      mobile: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.BOOLEAN
      },
      amount: {
        type: Sequelize.FLOAT
      },
      amountLeft: {
        type: Sequelize.FLOAT
      },
      promotor: {
        type: Sequelize.STRING
      },
      promotorId: {
        type: Sequelize.STRING
      },
      store: {
        type: Sequelize.STRING
      },
      storeId: {
        type: Sequelize.STRING
      },
      updated_by: {
        type: Sequelize.STRING
      }
    });
  
    return Promotion;
  };