
module.exports = (sequelize, DataTypes) => {
	const Storage = sequelize.define('storage', {
        storage_name: DataTypes.STRING,
		storage_type: DataTypes.STRING,
		storage_capacity: DataTypes.STRING,
		storage_occupancy: DataTypes.STRING,
        locid: DataTypes.STRING,
        createdAt: {
            allowNull: false,
            defaultValue: new Date(),
            type: DataTypes.DATE
          },
          updatedAt: {
            allowNull: false,
            defaultValue: new Date(),
            type: DataTypes.DATE
          }
	}, {});
	Storage.associate = function(models) {
		// Create associations here
	};
	return Storage;
};
