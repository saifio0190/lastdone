'use strict';

module.exports = (sequelize, DataTypes) => {
	const ordertransaction = sequelize.define('ordertransaction', {
		ns_order_id: DataTypes.STRING,
		bill_amount: DataTypes.FLOAT,
		razorpay_order_id: DataTypes.STRING,
		razorpay_payment_id: DataTypes.STRING,
		razorpay_signature: DataTypes.STRING,
		uid: DataTypes.STRING,
		refund: DataTypes.BOOLEAN,
		paytm_bank_txid: DataTypes.STRING,
		paytm_checksum: DataTypes.STRING,
		paytm_txn_id: DataTypes.STRING,
		gateway:DataTypes.STRING,
	}, {});
	ordertransaction.associate = function(models) {
		// Create associations here
		ordertransaction.hasMany(models.order);
	};
	return ordertransaction;
};
