'use strict';

module.exports = (sequelize, DataTypes) => {
	const userprofile = sequelize.define('userprofile', {
		name: DataTypes.STRING,
		email: DataTypes.STRING,
		phone: DataTypes.STRING,
		address: DataTypes.TEXT,
		dob: DataTypes.STRING,
		city: DataTypes.STRING,
		country: DataTypes.STRING,
	}, {});
	userprofile.associate = function(models) {
		// Create associations here
		userprofile.hasMany(models.order);
	};
	return userprofile;
};