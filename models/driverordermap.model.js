'use strict';

module.exports = (sequelize, DataTypes) => {
	const driverordermap = sequelize.define('driverordermap', {
        orderId: DataTypes.STRING,
        driverId:DataTypes.STRING,
        driverName:DataTypes.STRING,
        status:DataTypes.STRING, 
		address: DataTypes.STRING,
		phone: DataTypes.STRING,
		category: DataTypes.STRING,
		rank: DataTypes.STRING,
		storeid: DataTypes.STRING,
		ordercreationdate:DataTypes.STRING,
		customername:DataTypes.STRING,
	}, {});
	driverordermap.associate = function(models) {
		// Create associations here
	};
	return driverordermap;
};
