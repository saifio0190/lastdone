'use strict';

module.exports = (sequelize, DataTypes) => {
	const orderqueue = sequelize.define('orderqueue', {
		orderId: DataTypes.STRING,
		address: DataTypes.STRING,
		phone: DataTypes.STRING,
		category: DataTypes.STRING,
		rank: DataTypes.STRING,
		storeid: DataTypes.STRING,
		customername:DataTypes.STRING,
		paymentmode:DataTypes.STRING,
		billamt:DataTypes.STRING,
		ordercreationdate:DataTypes.STRING,
	}, {});
	orderqueue.associate = function(models) {
		// Create associations here
	};
	return orderqueue;
};
