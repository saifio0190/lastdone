/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */

module.exports = (sequelize, Sequelize) => {
    const promotion_opt = sequelize.define("promotion_opt", {
    
      mobile: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.BOOLEAN
      },
      opt_in: {
        type: Sequelize.BOOLEAN
      },
      opt_in_id: {
        type: Sequelize.STRING
      },
      opt_out: {
        type: Sequelize.BOOLEAN
      },
      response_JSON: {
        type: Sequelize.TEXT
      },
      greet: {
        type: Sequelize.BOOLEAN
      },
      response_greet_json: {
        type: Sequelize.TEXT
      },
      updated_by: {
        type: Sequelize.STRING
      }
    });
  
    return promotion_opt;
  };;

