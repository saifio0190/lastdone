/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const orders = sequelize.define('order', {
		order_id: DataTypes.STRING,
		bill_amount: DataTypes.FLOAT,
		status: { type: DataTypes.ENUM, values: ['RECEIVED', 'CONFIRMED', 'DISPATCHED', 'DELIVERED', 'CANCELLED'] },
		paid: DataTypes.BOOLEAN,
		payment_method: { type: DataTypes.ENUM, values: ['COD', 'CC', 'DB', 'UPI', 'EKPAY'] },
		userId: DataTypes.STRING,
		person:DataTypes.STRING,
		discount:DataTypes.STRING,
		obillamount:DataTypes.STRING,
	}, {});
	orders.associate = function(models) {
		orders.belongsTo(models.store, {
			foreignKey: {
				name: 'storeId',
				allowNull: false,
			},
			targetKey: 'id',
		});
		orders.belongsTo(models.user, {
			foreignKey: {
				name: 'userId',
				allowNull: false,
			},
			targetKey: 'id',
		});
		orders.belongsTo(models.userprofile, {
			foreignKey: {
				name: 'userprofileId',
				allowNull: false,
			},
			targetKey: 'id',
		});
		orders.belongsTo(models.ordertransaction, {
			foreignKey: {
				name: 'order_id',
				allowNull: false,
			},
			targetKey: 'ns_order_id',
		});
		orders.hasMany(models.orderitem);
		orders.hasMany(models.additional);
	};
	return orders;
};
