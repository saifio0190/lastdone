/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const feemap = sequelize.define('feemap', {
		value_slab_1: DataTypes.FLOAT,
		value_slab_2: DataTypes.FLOAT,
		value_slab_3: DataTypes.FLOAT,
		delivery_slab_1: DataTypes.FLOAT,
		delivery_slab_2: DataTypes.FLOAT,
		delivery_slab_3: DataTypes.FLOAT,
		delivery_min: DataTypes.FLOAT,
		coordination_slab_1: DataTypes.FLOAT,
		coordination_slab_2: DataTypes.FLOAT,
		coordination_slab_3: DataTypes.FLOAT,
		coordination_min: DataTypes.FLOAT,
		pack_slab_1: DataTypes.FLOAT,
		pack_slab_2: DataTypes.FLOAT,
		pack_slab_3: DataTypes.FLOAT,
		pack_min: DataTypes.FLOAT,
		misc_slab_1: DataTypes.FLOAT,
		misc_slab_2: DataTypes.FLOAT,
		misc_slab_3: DataTypes.FLOAT,
		misc_min: DataTypes.FLOAT,
		process_slab_1: DataTypes.FLOAT,
		process_slab_2: DataTypes.FLOAT,
		process_slab_3: DataTypes.FLOAT,
		process_min: DataTypes.FLOAT,
		disc_slab_1: DataTypes.FLOAT,
		disc_slab_2: DataTypes.FLOAT,
		disc_slab_3: DataTypes.FLOAT,
		disc_max_per: DataTypes.FLOAT,
		disc_slab_max_1: DataTypes.FLOAT,
		disc_slab_max_2: DataTypes.FLOAT,
		disc_slab_max_3: DataTypes.FLOAT,
		disc_slab_max: DataTypes.FLOAT,
		min_order_value: DataTypes.FLOAT,
		delivery_timeline: DataTypes.INTEGER, // in days
		cutoff_time: DataTypes.STRING, // format HH:mm:ss
		email: DataTypes.STRING,
		sunday_available: DataTypes.BOOLEAN,
	}, {});
	feemap.associate = function(models) {
		feemap.belongsTo(models.brand, {
			foreignKey: {
				name: 'brandId',
				allowNull: true,
			},
			targetKey: 'id',
		});
		feemap.belongsTo(models.store, {
			foreignKey: {
				name: 'storeId',
				allowNull: true,
			},
			targetKey: 'id',
		});
	};
	return feemap;
};
