'use strict';

module.exports = (sequelize, DataTypes) => {
	const storescan = sequelize.define('storescan', {
		name: DataTypes.STRING,
		displayName: DataTypes.STRING,
		group: DataTypes.STRING,
		category: DataTypes.STRING,
		barcode: DataTypes.STRING,
		type: DataTypes.STRING,
		company: DataTypes.STRING,
		brand: DataTypes.STRING,
		hsncode:DataTypes.STRING,
		tax_inc_cess:DataTypes.STRING,
		cgst:DataTypes.STRING,
		sgst:DataTypes.STRING,
		igst:DataTypes.STRING,
		cess:DataTypes.STRING,
		sku:DataTypes.STRING,
		coo:DataTypes.STRING,
		igstamt:DataTypes.STRING,
		description:DataTypes.STRING,
		expiry:DataTypes.STRING,
		product_type:DataTypes.STRING,
		qtyavailiable: DataTypes.STRING,
		url: DataTypes.STRING,
		box_id: DataTypes.STRING,
		storage_consume_unit: DataTypes.STRING,
		storage_consumed: DataTypes.STRING,
		storage_type: DataTypes.STRING,
		storage_id: DataTypes.INTEGER,
		count: DataTypes.INTEGER,
		price: DataTypes.FLOAT,
		discountPrice: DataTypes.FLOAT,
		endpoint: DataTypes.STRING,
		visible: DataTypes.BOOLEAN,
		newproduct: DataTypes.BOOLEAN,
		
	}, {});
	storescan.associate = function(models) {
		// Create associations here
	};
	return storescan;
};
