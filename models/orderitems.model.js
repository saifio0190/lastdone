'use strict';

module.exports = (sequelize, DataTypes) => {
	const orderitem = sequelize.define('orderitem', {
		available: DataTypes.BOOLEAN,
		quantity: DataTypes.INTEGER,
		quantity_available: DataTypes.INTEGER,
		item_price: DataTypes.FLOAT,
		tax: DataTypes.FLOAT,
		hsncode:DataTypes.STRING,
		tax_inc_cess:DataTypes.STRING,
		cgst:DataTypes.STRING,
		sgst:DataTypes.STRING,
		cess:DataTypes.STRING,
		deleted:DataTypes.STRING,
		cgstamt:DataTypes.STRING,
		sgstamt:DataTypes.STRING,
		cessamt:DataTypes.STRING,
		tax_inc_cess_amt:DataTypes.STRING,
		igst:DataTypes.STRING,
		igstamt:DataTypes.STRING,
		storeId:DataTypes.STRING,
		pretaxvalue:DataTypes.STRING,
		discountprice:DataTypes.STRING,
		discountval:DataTypes.STRING,
	}, {});

	orderitem.associate = function(models) {
		orderitem.belongsTo(models.order, {
			foreignKey: {
				name: 'orderId',
				allowNull: false,
			},
			targetKey: 'id',
		});
		orderitem.belongsTo(models.barcodemaster, {
			foreignKey: {
				name: 'barcodemasterId',
				allowNull: false,
			},
			targetKey: 'id',
		});
	};
	return orderitem;
};
