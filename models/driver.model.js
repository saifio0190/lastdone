'use strict';

module.exports = (sequelize, DataTypes) => {
	const driver = sequelize.define('driver', {
		drivername: DataTypes.STRING,
		phone_no: DataTypes.STRING,
		check_in: DataTypes.BOOLEAN,
		orderid: DataTypes.STRING,
		last_check_in: DataTypes.STRING,
		last_check_out: DataTypes.STRING,
		orderid: DataTypes.STRING,
        availiable: DataTypes.STRING,
        storeid:DataTypes.STRING,
	}, {});
	driver.associate = function(models) {
		// Create associations here
	};
	return driver;
};
