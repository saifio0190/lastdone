/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const admin = sequelize.define('admin', {
		name: DataTypes.STRING,
		email: DataTypes.STRING,
		password: DataTypes.STRING,
		active: DataTypes.BOOLEAN,
	}, {});
	admin.associate = function(models) {
	};
	return admin;
};

