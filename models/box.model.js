'use strict';

module.exports = (sequelize, DataTypes) => {
	const box = sequelize.define('box', {
		box_id: DataTypes.STRING,
		rcv_byte: DataTypes.INTEGER,
		state: DataTypes.INTEGER,
		store_url: DataTypes.STRING,
	}, {});
	box.associate = function(models) {
		// Create associations here
	};
	return box;
};
