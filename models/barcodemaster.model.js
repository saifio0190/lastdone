'use strict';

module.exports = (sequelize, DataTypes) => {
	const barcodemaster = sequelize.define('barcodemaster', {
		ProductVariant: DataTypes.STRING,
		ProductGroup: DataTypes.STRING,
		ProductCategory: DataTypes.STRING,
		BarCode: DataTypes.STRING,
		ProductType: DataTypes.STRING,
		url: DataTypes.STRING,
		Company: DataTypes.STRING,
		BrandName: DataTypes.STRING,
		Price: DataTypes.FLOAT,
		hsncode:DataTypes.STRING,
		tax_inc_cess:DataTypes.STRING,
		cgst:DataTypes.STRING,
		sgst:DataTypes.STRING,
		igst:DataTypes.STRING,
		cess:DataTypes.STRING,
		expiry:DataTypes.STRING,
		coi:DataTypes.STRING,
		storage_consume_unit: DataTypes.STRING,
		storage_type: DataTypes.STRING,
		storage_id: DataTypes.INTEGER,
		description:DataTypes.STRING,
		igstamt:DataTypes.STRING,
		image_front: DataTypes.STRING,
		image_back: DataTypes.STRING,
		image_1: DataTypes.STRING,
		image_2: DataTypes.STRING,
		image_3: DataTypes.STRING,
		image_4: DataTypes.STRING,
		vision_text_front: DataTypes.TEXT,
		vision_text_back: DataTypes.TEXT,
	}, {});
	barcodemaster.associate = function(models) {
		barcodemaster.hasMany(models.orderitem);
	};
	return barcodemaster;
};
