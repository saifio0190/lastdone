'use strict';

module.exports = (sequelize, DataTypes) => {
	const inventory = sequelize.define('inventory', {
		name: DataTypes.STRING,
		displayName: DataTypes.STRING,
		group: DataTypes.STRING,
		category: DataTypes.STRING,
		barcode: DataTypes.STRING,
		type: DataTypes.STRING,
		company: DataTypes.STRING,
		brand: DataTypes.STRING,
		url: DataTypes.STRING,
		box_id: DataTypes.STRING,
		count: DataTypes.INTEGER,
		price: DataTypes.FLOAT,
		discountPrice: DataTypes.FLOAT,
		endpoint: DataTypes.STRING,
		visible: DataTypes.BOOLEAN,
        newproduct: DataTypes.BOOLEAN,
        qtytotal:DataTypes.STRING,
        qtyshipped:DataTypes.STRING,
        qtyavailiable:DataTypes.STRING,
		qtyminimum:DataTypes.STRING,
		locationname:DataTypes.STRING,
        locationid:DataTypes.STRING,
	}, {});
	inventory.associate = function(models) {
		// Create associations here
	};
	return inventory;
};
