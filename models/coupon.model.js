'use strict';

module.exports = (sequelize, DataTypes) => {
	const coupon = sequelize.define('coupon', {
		name: DataTypes.STRING,
		value: DataTypes.STRING,
		status: DataTypes.STRING,
        expiry: DataTypes.STRING,
        c_type: DataTypes.STRING,
        mov: DataTypes.STRING,
	}, {});
	coupon.associate = function(models) {
		// Create associations here
	};
	return coupon;
};
