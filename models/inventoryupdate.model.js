'use strict';

module.exports = (sequelize, DataTypes) => {
	const inventoryupdate = sequelize.define('inventoryupdate', {
		action: DataTypes.STRING,
		barcode: DataTypes.STRING,
		box_id: DataTypes.STRING,
        prev_data: DataTypes.STRING,
        cur_data: DataTypes.STRING,
	}, {});
	inventoryupdate.associate = function(models) {
		// Create associations here
	};
	return inventoryupdate;
};
