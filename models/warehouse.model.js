'use strict';

module.exports = (sequelize, DataTypes) => {
	const warehouse = sequelize.define('warehouse', {
		name: DataTypes.STRING,
		address: DataTypes.STRING,
		phone: DataTypes.STRING,
		locid: DataTypes.STRING,
	}, {});
	warehouse.associate = function(models) {
		// Create associations here
	};
	return warehouse;
};
