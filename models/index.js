/* eslint-disable key-spacing */
/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
'use strict';

const Sequelize = require('sequelize');
const env = process.env.ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

let sequelize;
if (config.use_env_variable) {
	sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
	sequelize = new Sequelize(
		config.database,
		config.username,
		config.password,
		config,
	);
};

const db = {
	admin: sequelize.import('./admin.model'),
	brand: sequelize.import('./brand.model'),
	additional: sequelize.import('./additional.model'),
	feemap: sequelize.import('./feemap.model'),
	box: sequelize.import('./box.model'),
	store: sequelize.import('./store.model'),
	order: sequelize.import('./orders.model'),
	orderupdate: sequelize.import('./orderupdate.model'),
	orderitem: sequelize.import('./orderitems.model'),
	shortlink: sequelize.import('./shortlink.model'),
	storescan: sequelize.import('./storescan.model'),
	inventory: sequelize.import('./inventory.model'),
	orderqueue: sequelize.import('./orderqueue.model'),
	driver: sequelize.import('./driver.model'),
	driverL: sequelize.import('./driverL.model'),
	driverordermap:sequelize.import('./driverordermap.model'),
	inventoryupdate: sequelize.import('./inventoryupdate.model'),
	coupon: sequelize.import('./coupon.model'),
	warehouse: sequelize.import('./warehouse.model'),
	storage: sequelize.import('./storage.model'),
	packingpeople: sequelize.import('./packingpeople.model'),
	barcodemaster: sequelize.import('./barcodemaster.model'),
	storeadmin: sequelize.import('./storeadmin.model'),
	subscriber: sequelize.import('./subscriber.model'),
	user: sequelize.import('./user.model'),
	userprofile: sequelize.import('./userprofile.model'),
	ordertransaction: sequelize.import('./ordertransactions.model'),
	nearstorefee: sequelize.import('./nearstorefee.model'),
};

Object.keys(db).forEach((modelName) => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.promoOpt = require("./promoOpt.model.js")(sequelize, Sequelize);
db.promotion = require("./promotion.model.js")(sequelize, Sequelize);

module.exports = db;
