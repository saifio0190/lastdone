/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const brand = sequelize.define('brand', {
		name: DataTypes.STRING,
		brand_id: DataTypes.STRING,
		store_id: DataTypes.STRING,
	}, {});
	brand.associate = function(models) {
		brand.hasOne(models.feemap);
		brand.hasMany(models.additional);
	};
	return brand;
};
