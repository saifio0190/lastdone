/* eslint-disable object-curly-spacing */
/* eslint-disable max-len */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const additional = sequelize.define('additional', {
		type_of_fees: DataTypes.INTEGER,
		amount: DataTypes.FLOAT,
		tax_value: DataTypes.FLOAT,
		charged_by: DataTypes.STRING,
		reason: DataTypes.TEXT,
	}, {});
	additional.associate = function(models) {
		additional.belongsTo(models.order, {
			foreignKey: {
				name: 'orderId',
				allowNull: false,
			},
			targetKey: 'id',
		});
		additional.belongsTo(models.store, {
			foreignKey: {
				name: 'storeId',
				allowNull: true,
			},
			targetKey: 'id',
		});
		additional.belongsTo(models.brand, {
			foreignKey: {
				name: 'brandId',
				allowNull: true,
			},
			targetKey: 'id',
		});
	};
	return additional;
};
