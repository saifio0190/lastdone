'use strict';

module.exports = (sequelize, DataTypes) => {
	const store = sequelize.define('store', {
		name: DataTypes.STRING,
		store_id: DataTypes.STRING,
		address: DataTypes.STRING,
		url: DataTypes.STRING,
		// eslint-disable-next-line new-cap
		boxes: DataTypes.ARRAY(DataTypes.STRING),
		active: DataTypes.BOOLEAN,
	}, {});
	store.associate = function(models) {
		store.hasOne(models.shortlink);
		store.hasOne(models.feemap);
		store.hasMany(models.additional);
	};
	return store;
};
