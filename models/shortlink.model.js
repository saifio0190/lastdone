'use strict';

module.exports = (sequelize, DataTypes) => {
	const shortlink = sequelize.define('shortlink', {
		shortlink: {
			type: DataTypes.STRING,
			unique: true,
		},
	}, {});
	shortlink.associate = function(models) {
		shortlink.belongsTo(models.store);
	};
	return shortlink;
};
