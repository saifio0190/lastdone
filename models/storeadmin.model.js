/* eslint-disable object-curly-spacing */
'use strict';

module.exports = (sequelize, DataTypes) => {
	const storeAdmin = sequelize.define('storeadmin', {
		name: DataTypes.STRING,
		username: { type: DataTypes.STRING, unique: true },
		password: DataTypes.STRING,
		active: DataTypes.BOOLEAN,
		role: DataTypes.INTEGER,
		number: { type: DataTypes.STRING, unique: true },
		push_token: DataTypes.STRING,
	}, {});
	storeAdmin.associate = function(models) {
		storeAdmin.belongsTo(models.store, {
			foreignKey: {
				name: 'storeId',
				allowNull: false,
			},
			targetKey: 'id',
			onDelete: 'CASCADE',
		});
	};
	return storeAdmin;
};
