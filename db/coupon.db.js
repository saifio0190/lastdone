/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
const models        = require('../models');
const sequelize     = require('sequelize');
const QueryTypes    = sequelize.QueryTypes;
const Op            = sequelize.Op;
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/product.db.js');
logger.level        = log4js.levels.INFO.levelStr;




/**
 * This function check if a given barcode is already
 * being used
 *
 * @param {String} barcode product barcode to check
 * @return {Boolean} indication whether the barcode exists or not
 * @retval true barcode does not exists and can be assigned to new product
 * @retval false barcode is in use, do not assign to another product
 */
async function getcouponType() {
	try {
        const type = models.coupon.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('c_type')), 'c_type'],
			],
			order: ['c_type'],
		});
		return type;
	} catch (err) {
		logger.error(err);
		return false;
	}
}
async function getcoupon(id) {
    var result = {status:0,data:'failed'};
	try {
        const coupon = await models.coupon.findOne({
            where: {
                id: {
                    [Op.eq]: id,
                },         
            },
        });
		return coupon;
	} catch (err) {
		logger.error(err);
		return false;
	}
}
async function updateCouponById(updateData, id){
	var result = {status:0,data:'failed'};
	console.log(updateData,'updateData');
	console.log(id,'id');
	var updated =  models.coupon.update(
		updateData,
		 { where: { 
			 id: id,
		  } 
		 }
	   )
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
	
	  return updated;
}
async function updateStoreProductByBarcode(updateData, barcode, whlocation){
	var result = {status:0,data:'failed'};
	console.log(updateData,'updateDataStore');
	console.log(barcode,'barcode');
	console.log(whlocation,'whlocation');
	var updated =  models.storescan.update(
		updateData,
		 { where: { 
			 barcode: barcode,
			 box_id: whlocation,
		  } 
		 }
	   )
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
	  return updated;
}
async function addCoupon(insertData, name){
	var result = {status:0,data:'failed'};
    console.log('name',name)
    console.log('insertData',insertData)
var dataR =  await	models.coupon.findOne({
    where: {
        name: {
            [Op.eq]: name,
        },
    }
    });
    if(dataR){

        return true;
    }
    console.log('this data',dataR)
    console.log('this data',typeof(dataR));

	var updated =  models.coupon.create(insertData)
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });		
		
	  return updated;
}




async function getAllCoupon(){
    try {


        const coupons = await models.coupon.findAll({
            where: {
                value: {
                    [Op.ne]: null,
                },
                c_type: {
                    [Op.ne]: null,
                },
            },
            order: [['createdAt','DESC']],
        });
        const type = models.coupon.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('c_type')), 'c_type'],
			],
			order: ['c_type'],
		});

		const payload = await Promise.all([type, coupons]);
		return payload;
	} catch (err) {
		logger.error(err);
		return null;
	}

 

}



module.exports = {
	getcouponType,
	getcoupon,
	getAllCoupon,
	addCoupon,
	updateCouponById,

};

