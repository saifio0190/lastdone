
module.exports = {
	orders: require('./order.db'),
	inventory: require('./inventory.db'),
	coupon: require('./coupon.db'),
	store: require('./store.db'),
	promoOpt: require('./promoOpt.db'),
	admin: require('./admin.db'),
	shortlink: require('./shortlinks.db'),
	product: require('./product.db'),
	logistic: require('./logistic.db'),
	whatsapp: require('./whatsapp.db'),
	additional: require('./additional.db'),
};
