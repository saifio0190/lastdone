/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
const models        = require('../models');
const Op            = require('sequelize').Op;
const log4js        = require('log4js');
const { compareSync } = require('bcryptjs');
const logger        = log4js.getLogger('db/order.db.js');
logger.level        = log4js.levels.INFO.levelStr;

/**
 * This function returns number of orders by
 * day on near.store
 * @return {JSON} payload
 */
async function getNumberOfOrderByDay() {

}


/**
 * this function fetches dashboard info
 * data
 * @return {JSON} payload
 */


/**
 * Get all orders
 * @param {Number} storeId unique db storeId
 * @param {String} status Order status
 * @param {String} paidStatus Order's paid status (pending / paid)
 */

      async function getAllPromos(from, to){
        
        const query = `SELECT * FROM promotions WHERE "createdAt" > '${from}' AND "createdAt" < '${to}'`;
        
       data = await models.sequelize.query(query);
       return data[0];
      }

     async function getAllPromoOpts(){

const models        = require('../models');
const query = `SELECT promotions.id, promotions.status, promotions.amount, promotions."amountLeft", promotion_opts."mobile", promotion_opts."status" as optstatus, promotion_opts."opt_in", promotion_opts."opt_in_id", promotion_opts."greet", promotion_opts."createdAt" FROM promotions  JOIN promotion_opts ON promotions.mobile = promotion_opts."mobile" 
             WHERE promotions.mobile is NOT NULL AND promotion_opts."status"=true   ORDER BY promotion_opts."createdAt" DESC`;
			data = await models.sequelize.query(query);
// const allPromoOpt = await models.promoOpt.findAll({
//             where: {
//                 status: {
//                     [Op.eq]: true,
//                 },  
//                 greet: {
//                   [Op.eq]: true,
//               },          
      
//             },
//             order: [
//                 ['id', 'DESC'],
                
//             ],
//         });
//         return allPromoOpt;
        return data[0];
      }

      var updatePromoGreet = async function(updateData){
        const models        = require('../models');
        var updated = {status:0,data:'Something wen wrong'};
          console.log('upda',updateData);
         var updated = models.promoOpt.update(
            updateData,
             { where: { opt_in_id: updateData.opt_in_id} }
           )
        .then(data => {
         result = {status:1,data:data};
          return result;  
         })
         .catch(err => {
             result = {status:0,data:'failed'};
             return result;
         });
    
         return updated;
      }








module.exports = {
    getAllPromoOpts,
    getAllPromos,
	updatePromoGreet,
};
