/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
const models        = require('../models');
const Op            = require('sequelize').Op;
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/additional.db.js');
logger.level        = log4js.levels.INFO.levelStr;


/**
 * this function fetchs all additional charges
 * by brand and by store to be displayed in a
 * table format
 */
async function getAllAdditionals() {
	try {
		const additionals = await models.feemap.findAll({
			include: [
				{
					model: models.store,
				},
				{
					model: models.brand,
				},
			],
		});
		logger.info(`found ${additionals.length} row(s)`);
		return additionals;
	} catch (err) {
		logger.error(err);
		return [];
	}
}

/**
 * this function fetchs one additional charges
 * for brand/store to be displayed in a
 * editable format
 *
 * @param {Number} id additional db id
 */
async function getAdditional(id) {
	try {
		const additionals = await models.feemap.findOne({
			where: {
				id: {
					[Op.eq]: id,
				},
			},
			include: [
				{
					model: models.store,
				},
				{
					model: models.brand,
				},
			],
		});
		if (!additionals) {
			logger.info(`unable to find additionals with id ${id}`);
			return {};
		}
		logger.info(`found additional with id ${id}`);
		return additionals;
	} catch (err) {
		logger.error(err);
		return {};
	}
}

module.exports = {
	getAdditional,
	getAllAdditionals,
};

