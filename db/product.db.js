/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
const models        = require('../models');
const sequelize     = require('sequelize');
const QueryTypes    = sequelize.QueryTypes;
const Op            = sequelize.Op;
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/product.db.js');
logger.level        = log4js.levels.INFO.levelStr;

/**
 * this function returns distinct values for
 * store name, product category, product group,
 * company and brand name.
 */
async function getAllInfoForProductAddition() {
	try {
		const group = models.barcodemaster.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('ProductGroup')), 'ProductGroup'],
			],
			order: ['ProductGroup'],
		});
		const category = models.barcodemaster.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('ProductCategory')), 'ProductCategory'],
			],
			order: ['ProductCategory'],
		});
		const company = models.barcodemaster.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('Company')), 'Company'],
			],
			order: ['Company'],
		});
		const brand = models.barcodemaster.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('BrandName')), 'BrandName'],
			],
			order: ['BrandName'],
		});
		const ProductType = models.barcodemaster.findAll({
			attributes: [
				[sequelize.fn('DISTINCT', sequelize.col('ProductType')), 'ProductType'],
			],
			order: ['ProductType'],
		});
		const store = models.store.findAll({
			attributes: ['id', 'name'],
			order: ['name'],
		});
		const location = models.warehouse.findAll({
			attributes: ['locid', 'name'],
			order: ['name'],
		});
		const storage = models.storage.findAll({
			attributes: ['locid', 'storage_name','storage_type','storage_capacity','storage_occupancy','id'],
			order: ['id'],
		});

		const payload = await Promise.all([group, category, company, brand, store,location, ProductType, storage]);
		return payload;
	} catch (err) {
		logger.error(err);
		return null;
	}
}


/**
 * This function check if a given barcode is already
 * being used
 *
 * @param {String} barcode product barcode to check
 * @return {Boolean} indication whether the barcode exists or not
 * @retval true barcode does not exists and can be assigned to new product
 * @retval false barcode is in use, do not assign to another product
 */
async function barcodeCheck(barcode) {
	try {
		const product = await models.barcodemaster.findOne({
			where: {
				BarCode: {
					[Op.eq]: barcode,
				},
			},
		});
		if (!product) {
			logger.info(`product does not exist with ${barcode}`);
			return true;
		}
		logger.info(`product exists with ${barcode}`);
		return false;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


/**
 * this function takes a new product info, inserts into
 * the master and also store catalogue mentioned in the
 * payload
 *
 * @param {JSON} payload request body
 * @param {String} payload.name product name
 * @param {String} payload.group product group
 * @param {String} payload.category product category
 * @param {String} payload.barcode product barcode
 * @param {String} payload.ptype product type
 * @param {String} payload.image product image url
 * @param {String} payload.company product company
 * @param {String} payload.brand product brand
 * @param {Float}  payload.price product price
 * @param {Float}  payload.discountPrice product discont Price
 * @param {Number} payload.store store for which product is to be added
 *                 to catalogue
 */
async function addProductsToCatalogue(payload) {
	try {
		const _store = await models.store.findOne({
			where: {
				id: {
					[Op.eq]: payload.store,
				},
			},
		});
		if (!_store) {
			logger.error(`no store with id ${payload.store}`);
			return false;
		}
		// Check if product already exists in master
		const _entry = await models.barcodemaster.findOne({
			where: {
				BarCode: {
					[Op.eq]: payload.barcode,
				},
			},
		});
		console.log(_entry);

		if (!_entry) {
			const master = await models.barcodemaster.create({
				id: models.sequelize.fn('nextval', 'barcodemasters_id_seq'),
				ProductVariant: payload.name,
				ProductGroup: payload.group,
				ProductCategory: payload.category,
				BarCode: payload.barcode,
				ProductType: payload.ptype,
				url: null,
				Company: payload.company,
				BrandName: payload.brand,
				Price: payload.price,
				image_front: payload.image,
			});
			if (process.env.ENV === 'production') {
				logger.info(`inserting in new_barcodemaster`);
				await models.sequelize.query('INSERT INTO new_barcodemaster values (:id, :name, :group, :category, :barcode, :ptype, null, :company, :brand, :price, current_timestamp, current_timestamp, :image, null, null, null, null, null, null, null)', {
					replacements: {
						id: master.id,
						name: payload.name,
						group: payload.group,
						category: payload.category,
						barcode: payload.barcode,
						ptype: payload.ptype,
						company: payload.company,
						brand: payload.brand,
						price: payload.price,
						image: payload.image,
					},
					type: QueryTypes.INSERT,
				});
			}
		} else {
			logger.info(`product already exists with barcode: ${payload.barcode}`);
			return false;
		}

		// check if product alreadt exists in catalogue table
		const _catalogue = await models.storescan.findOne({
			where: {
				barcode: {
					[Op.eq]: payload.barcode,
				},
				box_id: {
					[Op.or]: _store.boxes,
				},
			},
		});
		if (!_catalogue) {
			await models.storescan.create({
				name: payload.name,
				displayName: payload.name,
				group: payload.group,
				category: payload.category,
				barcode: payload.barcode,
				type: payload.ptype,
				company: payload.company,
				brand: payload.brand,
				url: payload.image,
				box_id: _store.boxes[0],
				count: 0,
				price: payload.price,
				discountPrice: payload.discountPrice,
				endpoint: 'send',
				visible: true,
				newproduct: false,
			});
			logger.info(`inserted product ${payload.name} with ${payload.barcode}`);
			return true;
		} else {
			logger.info(`${payload.barcode} already exists for ${_store.store_id}`);
			return false;
		}
	} catch (err) {
		logger.error(err);
		return false;
	}
}

module.exports = {
	barcodeCheck,
	addProductsToCatalogue,
	getAllInfoForProductAddition,
};

