/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/shortlink.db.js');
logger.level        = log4js.levels.INFO.levelStr;

/**
 * this function inserts new
 * shortlink
 * @param {String} shortlink Store shortlink
 * @return {Boolean} indication
 */
async function insertShortlink(shortlink) {
}

module.exports = {
	insertShortlink,
};
