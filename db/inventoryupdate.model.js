'use strict';

module.exports = (sequelize, DataTypes) => {
	const inventoryupdate = sequelize.define('inventoryupdate', {
		action: DataTypes.STRING,
		barcode: DataTypes.STRING,
		box_id: DataTypes.STRING,
        prev_data: DataTypes.TEXT,
        cur_data: DataTypes.TEXT,
	}, {});
	inventoryupdate.associate = function(models) {
		// Create associations here
	};
	return inventoryupdate;
};
