/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
const models        = require('../models');
const Op            = require('sequelize').Op;
const log4js        = require('log4js');
const fs        = require('fs');
const logger        = log4js.getLogger('db/store.db.js');
logger.level        = log4js.levels.INFO.levelStr;
const NodeGeocoder = require('node-geocoder');
const kmeans = require('node-kmeans');
const axios     = require('axios');
var querystring = require("querystring");
const { send } = require('process');
const db1        = require('../db');




const options = {
	provider: 'google',
   
	// Optional depending on the providers
	//fetch: customFetchImplementation,
	apiKey: 'AIzaSyDzguEWEhmgChzOdIyPClhz5mgLGueanvs', // for Mapquest, OpenCage, Google Premier
	formatter: null // 'gpx', 'string', ...
  };
   
  const geocoder = NodeGeocoder(options);
   



/**
 * Get all stores
 */
async function getAllStores() {
	try {
		const _stores = await models.warehouse.findAll({
			attributes: ['id', 'name','locid'],
		});
		logger.info(`found ${_stores.length} store(s)`);
		return _stores;
	} catch (err) {
		logger.error(err);
		return [];
	}
}


async function getAllStoresALl() {
	try {
		const _stores = await models.store.findAll({
			attributes: ['id', 'name'],
		});
		logger.info(`found ${_stores.length} store(s)`);
		return _stores;
	} catch (err) {
		logger.error(err);
		return [];
	}
}

/**
 * Get all stores with Shortlinks
 */
async function getAllStoresWithShortLinks() {
	try {
		const _stores = await models.store.findAll({
			attributes: ['id', 'name'],
			include: {
				model: models.shortlink,
			},
		});
		logger.info(`found ${_stores.length} store(s)`);
		return _stores;
	} catch (err) {
		logger.error(err);
		return [];
	}
}

/**
 * This function returns products in
 * a store
 * @param {Number} storeId Store Id
 */
async function getStoreCatalogue(storeId) {

	console.log(storeId);
	try {
		if (!storeId) {
			logger.info(`no storeid`);
			return [];
		}
		
		
		const products = await models.storescan.findAll({
			where: {
				box_id: {
					[Op.eq]: storeId,
				},
			},
		});
		

		console.log(products);
		return products;
	} catch (err) {
		logger.error(err);
		return [];
	}
}


async function updateDriverStatus(updateData,id){

		var result = {status:0,data:'failed'};
		console.log(id,updateData);
		var updated =  models.driver.update(
			updateData,
			 { where: { 
				 id: id,
			  } 
			 }
		   )
		   .then(data => {
			 result = {status:1,data:'success'};
			  return result;
			  
			 })
			 .catch(err => {
				 result = {status:0,data:'failed'};
				 return result;
			 });
			
		
		  return updated;
	}


async function getDrivers(storeId) {

	console.log(storeId);
	try {
		if (!storeId) {
			logger.info(`no storeid`);
			return [];
		}
		
		
		const drivers = await models.driver.findAll({
			where: {
				storeid: {
					[Op.eq]: storeId,
				},
			},
		});

		const driverscount = await models.driver.count({
			where: {
				storeid: {
					[Op.eq]: storeId,
				},
				availiable: {
					[Op.eq]: "true",
				},
			},
		});


		const queuecount = await models.orderqueue.count({});

		const queueorder = await models.orderqueue.findAll({});

		console.log(drivers);
		return {drivers,driverscount,queuecount,queueorder};
	} catch (err) {
		logger.error(err);
		return [];
	}
}



//assign driver logic


async function assigndrivers(store) {

	
	try {

			var orderarray = [];

			var driverarray = [];
	


			const queueorder= await models.orderqueue.findAll({
				
				order: [
					['rank', 'ASC'],
				],

			});

			// console.log(queueorder);

			// console.log(queueorder.length);

			if(queueorder.length == 0){

				return "false"
				
			}

			const driversav = await models.driver.findAll({
				where: {
					storeid: {
						[Op.eq]: store,
					},
					availiable: {
						[Op.eq]: "true",
					},
				},
			});


			for(i = 0;i< driversav.length;i++){

				driverarray.push({"id":driversav[i].id,"name":driversav[i].drivername,"phone":driversav[i].phone_no});


			}


			var slat  ;

			var slong ;


			for(i = 0;i< queueorder.length;i++){

			//	console.log(queueorder[i]);


				  // Using callback

				  const resp = await geocoder.geocode(queueorder[0].address);
					  const res = await geocoder.geocode(queueorder[i].address);
					  
					//   console.log(res[0].latitude);

					  slat = resp[0].latitude;

					//   console.log(res[0].longitude);

					  slong = resp[0].longitude;

					  orderarray.push({"orderid":queueorder[i].orderId , "latitude"  :res[0].latitude , "longitude":res[0].longitude,"address":queueorder[i].address,"ordercreationdate":queueorder[i].ordercreationdate,"storeid":queueorder[i].storeid,"phone":queueorder[i].phone,"customername":queueorder[i].customername,"paymentmode":queueorder[i].paymentmode,"billamt":queueorder[i].billamt });




			// 	const driverscount = await models.driver.count({
			// 	where: {
			// 		storeid: {
			// 			[Op.eq]: "239",
			// 		},
			// 		availiable: {
			// 			[Op.eq]: "true",
			// 		},
			// 	},
			// });

			// if(driverscount >= 1){





			}

			// console.log(slat);

			// console.log(slong);



			
			
			
				
					var data = orderarray;
					var driverss = driverarray;

					//data.length = 3;

					if(data.length < driverss.length){

						driverss.length = data.length;
					}

					//console.log(driverss.length);

					
					if (data.length==0 || driverss.length==0)
						return res.status(400).json({'message' : 'Error'});
					else {
						let vectors = new Array();
						for (let i = 0 ; i < data.length ; i++) {
							vectors[i] = [ data[i]['longitude'] , data[i]['latitude']];
						}
						kmeans.clusterize(vectors, {k: driverss.length}, (err,result) => {
							if (err) {

								console.log(err);
								//return res.status(400).json({'message' : 'Error'});
							}
							else {

								//console.log(result);
								var i = 0;
								var t = 0;
								var driversAssigned = [];
								var ordersAssigned = [];
								orderclub = [];
								for(i=0;i<result.length;i++) {
									ordersAssigned = [];
									
									for(t=0;t<result[i].clusterInd.length;t++) {

										

									//	console.log(data[result[i].clusterInd[t]].orderid);
										ordersAssigned.push({
											order_id : data[result[i].clusterInd[t]].orderid,
											address:data[result[i].clusterInd[t]].address,
											phone:data[result[i].clusterInd[t]].phone,
											customername:data[result[i].clusterInd[t]].customername,
											// paymentmode:data[result[i].clusterInd[t]].paymentmode,
											paymentmode:"COD",
											billamt:data[result[i].clusterInd[t]].billamt,
											storeid:data[result[i].clusterInd[t]].storeid,
											ordercreationdate:data[result[i].clusterInd[t]].ordercreationdate,

										});
									}

									//console.log(ordersAssigned);
									driversAssigned.push({
										driver_id : driverss[i].id,
										name : driverss[i].name,
										mobile:driverss[i].phone,
										ordersAssigned : ordersAssigned
									});
								}


								
								

								for(i=0;i<driversAssigned.length;i++){


									var newStr = "";
									var durl

									//console.log(orderclub.toString());


								//	console.log(driversAssigned[i].ordersAssigned.length);

									if(driversAssigned[i].ordersAssigned.length > 0){


										for(l=0;l<driversAssigned[i].ordersAssigned.length;l++){

											orderclub.push(driversAssigned[i].ordersAssigned[l].order_id);

										


										//	console.log(driversAssigned[i].ordersAssigned[l].order_id);
	
	
											var driverid = driversAssigned[i].driver_id;
										var drivername = driversAssigned[i].name;
										var orderid = driversAssigned[i].ordersAssigned[l].order_id;
										var status = "Dispatched";
										var address = driversAssigned[i].ordersAssigned[l].address;
										var phone = driversAssigned[i].ordersAssigned[l].phone;
										var customername = driversAssigned[i].ordersAssigned[l].customername;
										var paymentmode = driversAssigned[i].ordersAssigned[l].paymentmode;
										var billamt = driversAssigned[i].ordersAssigned[l].billamt;

										var storeid = driversAssigned[i].ordersAssigned[l].storeid;
										var ordercreationdate = driversAssigned[i].ordersAssigned[l].ordercreationdate;
										
										var formattedaddress = address.replace(/[^a-zA-Z ]/g, "")

										var url = `api/updatedeliverystatus?orderid=${orderid}`;

										 durl = encodeURIComponent(`api/backtohome?driverid=${driverid}`);

										 
										


										const _updated1 =  models.driver.update({ availiable : "false"  }, {
											where: {
												id: {
													[Op.eq]: driverid,
												},
											},
										});	

										models.orderqueue.destroy({
											where: {
												orderId:{

													[Op.eq]:orderid,

												},
											},
											//truncate: true
										  });
										

									//	var addmap = encodeURI(`https://www.google.com/maps?q=${formattedaddress}`); 

										var pluslink = formattedaddress.replace(/\s+/g, '+');

										



										var  finalmaplink = "https://www.google.com/maps?q="+pluslink;

										
										var fmap = encodeURIComponent(finalmaplink);

										var lisorder

										if(orderclub.length === 3){

											 lisorder = orderclub.toString()+"+%2C+0";


										}else if(orderclub.length === 2){

											lisorder = orderclub.toString()+"+%2C+0+%2C+0";

										}else if(orderclub.length === 1){

											lisorder = orderclub.toString()+"+%2C+0+%2C+0+%2C+0";

										}else{

											lisorder = orderclub.toString();

										}

										//var lisorder = orderclub.toString();

									//	var encodelistorder = encodeURIComponent(lisorder);

								

										 newStr = lisorder.replace(/,/g, '+%2C+');
										console.log(encodeURIComponent(newStr));
										var enstr = encodeURIComponent(newStr);

										var escapeadd =formattedaddress.replace(/&/g, '%26');

										console.log(address);
										



										

										var furl = encodeURIComponent(url);

										var addressf = encodeURIComponent(address);

										axios.get(`https://media.smsgupshup.com/GatewayAPI/rest?userid=2000195506&password=5C@U4UrB&method=SENDMESSAGE&msg=Order+ID+-+${orderid}%0ACustomer+Name+-+${customername}%0ACustomer+Mobile+-+${phone}%0ACustomer+Address+-+${addressf}%0AMap+Link+-+${fmap}%0APayment+Mode+-+${paymentmode}%0AAmount+to+be+Collected+-+INR+${billamt}&send_to=${driversAssigned[i].mobile}&msg_type=TEXT&isTemplate=true&buttonUrlParam=${furl}&v=1.1&format=json`)
										.then(function (response) {
										  // handle success
										  //console.log(response);
										})
										.catch(function (error) {
										  // handle error
										 //ß console.log(error);
										})
										.then(function () {
										  // always executed
										});


											try {
										const _drivercreate =  models.driverordermap.create({
											orderId: orderid,
											driverId: driverid,
											driverName: drivername,
											status: status,
											address: address,
											phone:phone,
											storeid:storeid,
											ordercreationdate: ordercreationdate,
										});
										
									} catch (error) {
										
									}
	
									
	
										}

										var sendurl = `https://media.smsgupshup.com/GatewayAPI/rest?userid=2000195506&password=5C@U4UrB&method=SENDMESSAGE&msg=The+Order+ID%27s+${newStr}+have+been+clubbed+together+for+delivery.+Please+click+the+button+below+once+they+have+been+delivered+and+you+are+back+at+the+home+location.&send_to=${driversAssigned[i].mobile}&msg_type=TEXT&isTemplate=true&buttonUrlParam=${durl}&v=1.1&format=json`;

										axios.get(`${sendurl}`)
										.then(function (response) {
										  // handle success
										  console.log(response);
										})
										.catch(function (error) {
										  // handle error
										 // console.log(error);
										})
										.then(function () {
										  // always executed
										});

										orderclub = [];

									}

							

									

								
								
								}

								return driversAssigned;
								//return res.status(200).json(driversAssigned);
							}
						});
					}
				
			

				



			


	
			// const driverscount = await models.driver.count({
			// 	where: {
			// 		storeid: {
			// 			[Op.eq]: "239",
			// 		},
			// 		availiable: {
			// 			[Op.eq]: "true",
			// 		},
			// 	},
			// });
	
		
		// const drivers = await models.driver.findAll({
		// 	where: {
		// 		storeid: {
		// 			[Op.eq]: storeId,
		// 		},
		// 	},
		// });

		// const driverscount = await models.driver.count({
		// 	where: {
		// 		storeid: {
		// 			[Op.eq]: storeId,
		// 		},
		// 		availiable: {
		// 			[Op.eq]: "true",
		// 		},
		// 	},
		// });


		

		
		//return {drivers,driverscount,queuecount};
	} catch (err) {
		logger.error(err);
		return [];
	}
}



async function capturesearch() {

	
	const query = `select * from searchkeywords`;
data = await models.sequelize.query(query);
console.log(data);
return data[0]
}


async function getAllStoreCatalogue() {

	try {
		
		
		const products = await models.storescan.findAll({
			where: {
				box_id: {
					[Op.ne]: null,
				},
			},
		});

	// 	console.log('heressqqqer');
		 
    //     const query = `select * FROM storescans WHERE box_id='e0512496NSKI' and name NOT IN(select name FROM storescans GROUP BY name HAVING COUNT(name)> 1)`;  
    //    data = await models.sequelize.query(query);
    //    return data[0];

		
		return products;
	} catch (err) {
		logger.error(err);
		return [];
	}
}


/**
 * The function sets the visible flag
 * for a product with barcode to value of visible
 * @param {Number} storeId StoreId of product
 * @param {String} barcode product barcode
 * @param {Boolean} visible product visibility
 * 
 * 
 */

function writePromoData(lineData){
	var result = {status:0,data:'failed'};
	
	var promo = new Promise((resolve, reject) => {
		
	const stream = fs.createWriteStream('./addUpdatelogInventory.csv',{flags:'a'});
	stream.write(lineData +"\r\n ",function(){
		result = {status:1,data:'success'};
		
	return resolve(result);
	})
	});
	return promo;
	}
async function updateProductVisibility(storeId, barcode, visible) {
	try {

		console.log(storeId,'store');
		console.log(barcode,'barcode');
		// const _store = await models.store.findOne({
		// 	where: {
		// 		id: {
		// 			[Op.eq]: storeId,
		// 		},
		// 	},
		// 	attributes: ['boxes'],
		// });
		// if (!_store) {
		// 	logger.info(`no store with id ${storeId}`);
		// 	return [];
		// }
		await models.storescan.update({visible: visible}, {
			where: {
				box_id: {
					[Op.eq]: storeId,
				},
				barcode: {
					[Op.eq]: barcode,
				},
			},
		});
		var insertData  = {};
		insertData.action = 'update';
		insertData.barcode = barcode;
		insertData.box_id = storeId;
		insertData.cur_data = JSON.stringify({visible: visible});
		insertData.prev_data = '';
		const updateLog =  await models.inventoryupdate.create(insertData);
		var data = "updateData -,"+JSON.stringify({visible: visible})+","+new Date();
		writePromoData(data);
		logger.info(`updated visible to ${visible} for ${barcode} for store ${storeId}`);
		return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


/**
 * The function sets the visible flag
 * for an entire store to value of visible
 * @param {Number} storeId StoreId of product
 * @param {Boolean} visible product visibility
 */
async function toggleVisibiliyForStoreProducts(storeId, visible) {
	try {
		let _store = await models.store.findOne({
			where: {
				id: {
					[Op.eq]: storeId,
				},
			},
			attributes: ['boxes'],
		});
		if (!_store) {
			logger.info(`no store with id ${storeId}`);
			return [];
		}
		await models.storescan.update({visible: visible}, {
			where: {
				box_id: {
					[Op.or]: _store.boxes,
				},
			},
		});
		logger.info(`updated visible to ${visible} for store ${storeId}`);
		return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


async function insertDriverStatus(insertData){
	const updateLog =  await models.driverL.create(insertData);
	return updateLog;
}

async function updatedeliverystatus(orderid) {
	try {
	
		await models.order.update({status: "DELIVERED"}, {
			where: {
				order_id: {
					[Op.eq]: orderid,
				},
			},
		});

		await models.driverordermap.update({status: "DELIVERED"}, {
			where: {
				orderId: {
					[Op.eq]: orderid,
				},
			},
		});
		logger.info(`updated status to ${orderid} for order ${orderid}`);
		return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


async function updatebacktohomestatus(orderid) {
	try {
	
		await models.driver.update({availiable: "true"}, {
			where: {
				id: {
					[Op.eq]: orderid,
				},
			},
		});

	
		logger.info(`updated status to ${orderid} for order ${orderid}`);
		return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


/**
 * Get product from store catalgoue
 * @param {String} barcode product barcode
 * @param {Number} storeId Store Id
 */
async function getProductFromCatalogue(barcode, storeId) {
	try {
		if (!storeId) {
			logger.info(`no storeid`);
			return [];
		}
		let _store = await models.store.findOne({
			where: {
				id: {
					[Op.eq]: '175',
				},
			},
			attributes: ['boxes'],
		});
		if (!_store) {
			logger.info(`no store with id ${storeId}`);
			return [];
		}
		const product = await models.storescan.findOne({
			where: {
				box_id: {
					[Op.or]: _store.boxes,
				},
				barcode: {
					[Op.eq]: barcode,
				},
			},
		});
		return product;
	} catch (err) {
		logger.error(err);
		return {};
	}
}


/**
 * This function updates products displayName, discountPrice
 * and sale count values
 * @param {Object} payload Update payload
 * @param {String} payload.displayName Product display name
 * @param {Float} payload.discountPrice Product discount price
 * @param {Number} payload.saleCount Product sale count
 * @param {Number} payload.storeId storeId
 * @param {String} payload.barcode Product barcode
 */
async function updateProductInCatalogue(payload) {
	let _store = await models.store.findOne({
		where: {
			id: {
				[Op.eq]: payload.storeId,
			},
		},
		attributes: ['boxes'],
	});
	if (!_store) {
		logger.info(`no store with id ${storeId}`);
		return [];
	}
	return models.storescan.findOne({
		where: {
			box_id: {
				[Op.or]: _store.boxes,
			},
			barcode: {
				[Op.eq]: payload.barcode,
			},
		},
	}).then((product) => {
		return product.update({
			displayName: payload.displayName,
			discountPrice: payload.discountPrice,
			count: payload.saleCount,
			price: payload.price,
		});
	}).then((product) => {
		logger.info(`updated product to ${JSON.stringify(payload)} for store ${payload.storeId}`);
		return true;
	}).catch((err) => {
		logger.error(err);
		return false;
	});
}


module.exports = {
	getAllStores,
	getAllStoresWithShortLinks,
	getStoreCatalogue,
	getAllStoreCatalogue,
	updateProductVisibility,
	toggleVisibiliyForStoreProducts,
	getProductFromCatalogue,
	updateProductInCatalogue,
	updateDriverStatus,
	insertDriverStatus,
	getAllStoresALl,
	capturesearch,
	getDrivers,
	assigndrivers,
	updatedeliverystatus,
	updatebacktohomestatus,
};
