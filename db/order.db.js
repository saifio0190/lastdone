/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */

const Op            = require('sequelize').Op;
const models        =  require('../models');
const log4js        = require('log4js');
const { compareSync } = require('bcryptjs');
const { order } = require('../models');
const db = require('../models');
const logger        = log4js.getLogger('db/order.db.js');
logger.level        = log4js.levels.INFO.levelStr;
const db1        = require('../db');

/**
 * This function returns number of orders by
 * day on near.store
 * @return {JSON} payload
 */
 async function getNumberOfOrderByDay(dateq) {
	const orders = await models.order.findAndCountAll({
		where: {
			paid: {
				[Op.eq]: true,
			},
		},
	});
	return orders;
	
}


/**
 * this function fetches dashboard info
 * data
 * @return {JSON} payload
 */
async function dashboardData() {

	var lastthreebill = 0;
	try {
		let payload = {};
		const orders = await models.order.findAndCountAll({
			where: {
				paid: {
					[Op.eq]: true,
				},
			},
		});

		const ordersthree = await models.order.findAll({
			where: {
				paid: {
					[Op.eq]: true,
				},
			},
			limit: 3,
			order: [ [ 'createdAt', 'DESC' ]]		
		});

		// console.log(ordersthree);


		ordersthree.forEach( async element => {
			//console.log(element.bi);
			
					lastthreebill += element.bill_amount;
			
			
							
							})

		
		const userss = await models.user.findAndCountAll({
		
		});
		payload.orders_count = orders.count;
		payload.usercount = userss.count;
		payload.usercount = userss.count;
		payload.threebill = lastthreebill;
		const totalBill = await models.order.sum('bill_amount', {
			where: {
				paid: {
					[Op.eq]: true,
				},
				status: {
					[Op.ne]: 'CANCELLED',
				},
			},
		});
		payload.bill_amount_total = totalBill;
		const payment = await models.order.findAndCountAll({
			where: {
				paid: {
					[Op.eq]: false,
				},
			},
		});
		payload.pending_payment = payment.count;
		const order = await models.order.findAndCountAll({
			where: {
				status: {
					[Op.eq]: 'RECEIVED',
				},
				paid: {
					[Op.eq]: true,
				},
			},
		});
		payload.new_orders = order.count;
		return payload;
	} catch (err) {
		logger.error(err);
		const payload = {
			orders_count: 0,
			bill_amount_total: 0,
			pending_payment: 0,
			new_orders: 0,
		};
		return payload;
	}
}

/**
 * Get all orders
 * @param {Number} storeId unique db storeId
 * @param {String} status Order status
 * @param {String} paidStatus Order's paid status (pending / paid)
 */
async function getAllOrders(storeId, status, paidStatus,old) {
	try {
		let data = null;



	
		
		let statusQ = (status == undefined) ? '' : `AND orders.status = '${status}'`;
		let paidStatusQ = (paidStatus == undefined) ? '' : ((paidStatus == '1') ? 'AND orders.paid = true' : 'AND orders.paid = false');
		if (!storeId) {
			data = [[]];
		} else {
			logger.info(storeId);
			console.log('this quey',old);
			const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount, ordertransactions.razorpay_payment_id,ordertransactions.gateway, ordertransactions.paytm_bank_txid, stores.name, users."email", orders."createdAt",orders.person FROM orders  JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" 
			 JOIN users ON orders."userId" = users.id JOIN stores ON  stores.id = orderstorexrefs."storeId" JOIN ordertransactions 
			ON orders.order_id = ordertransactions.ns_order_id   WHERE orderstorexrefs."storeId" = ${storeId} ${statusQ} ${paidStatusQ} ORDER BY orders."createdAt" DESC`;
			data = await models.sequelize.query(query);
			console.log('yes bro here');

			if(old =='yes'){
				
				const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount, ordertransactions.razorpay_payment_id, ordertransactions.paytm_bank_txid, stores.name, users."email", orders."createdAt" FROM orders JOIN ordertransactions 
			 ON orders.order_id = ordertransactions.ns_order_id  JOIN users
			ON orders."userId" = users.id JOIN stores ON orders."storeId" = stores.id WHERE "storeId" = ${storeId} ${statusQ} ${paidStatusQ} ORDER BY orders."createdAt" DESC`;
			 data = await models.sequelize.query(query);
			}
		}
		const orders = data[0];
	//	console.log('orders',orders);
		logger.info(`found ${orders.length} order(s)`);
		return orders;
	} catch (err) {
		logger.error(err);
		return [];
	}
}
async function getAllUnpaidOrders(storeId, status, paidStatus,old) {
	try {
		let data = null;



	
		
		let statusQ = (status == undefined) ? '' : `AND orders.status = '${status}'`;
		let paidStatusQ = (paidStatus == undefined) ? '' : ((paidStatus == '1') ? 'AND orders.paid = true' : 'AND orders.paid = false');
		if (!storeId) {
			data = [[]];
		} else {
			logger.info(storeId);
			console.log('this quey',old);
			const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount,  stores.name, users."email", orders."createdAt",orders.person FROM orders  JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" 
			 JOIN users ON orders."userId" = users.id JOIN stores ON  stores.id = orderstorexrefs."storeId"   WHERE orderstorexrefs."storeId" = ${storeId} ${statusQ} ${paidStatusQ} AND paid=false ORDER BY orders."createdAt" DESC`;
			// const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount, ordertransactions.razorpay_payment_id, ordertransactions.paytm_bank_txid, stores.name, users."email", orders."createdAt",orders.person FROM orders  JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" 
			//  JOIN users ON orders."userId" = users.id JOIN stores ON  stores.id = orderstorexrefs."storeId" JOIN ordertransactions 
			// ON orders.order_id = ordertransactions.ns_order_id   WHERE orderstorexrefs."storeId" = ${storeId} ${statusQ} ${paidStatusQ} ORDER BY orders."createdAt" DESC`;
			data = await models.sequelize.query(query);

		}
		const orders = data[0];
	
		logger.info(`found ${orders.length} order(s)`);
		return orders;
	} catch (err) {
		logger.error(err);
		return [];
	}
}




//Get all orderitems


async function getAllOrderitems() {
	try {

			//const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount,  stores.name, users."email", orders."createdAt",orders.person FROM orders  JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" 
			 //JOIN users ON orders."userId" = users.id JOIN stores ON  stores.id = orderstorexrefs."storeId"   WHERE orderstorexrefs."storeId" = ${storeId} ${statusQ} ${paidStatusQ} AND paid=false ORDER BY orders."createdAt" DESC`;
			// const query = `SELECT orders.order_id, orders.status, orders.paid, orders.bill_amount, ordertransactions.razorpay_payment_id, ordertransactions.paytm_bank_txid, stores.name, users."email", orders."createdAt",orders.person FROM orders  JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" 
			//  JOIN users ON orders."userId" = users.id JOIN stores ON  stores.id = orderstorexrefs."storeId" JOIN ordertransactions 
			// ON orders.order_id = ordertransactions.ns_order_id   WHERE orderstorexrefs."storeId" = ${storeId} ${statusQ} ${paidStatusQ} ORDER BY orders."createdAt" DESC`;
			
			const query = `select order_id , name , brand , quantity ,orderitems."storeId", orders."createdAt"  from orders inner join orderitems ON orderitems."orderId" = orders.id where orders.status = 'DELIVERED'`;
			data = await models.sequelize.query(query);

		
		const orders = data[0];
	
		logger.info(`found ${orders.length} order(s)`);
		return orders;
	} catch (err) {
		logger.error(err);
		return [];
	}
}


//get all packing persons 


async function getallpackingpersons(storeid){


	console.log(storeid);

	const packingperson = await models.packingpeople.findAll({
		where: {
			locationid: {
				[Op.eq]: storeid,
			},
		},
		
		
	});



	return packingperson;




}
async function todaysOrder(){
	const TODAY_START = new Date().setHours(0, 0, 0, 0);
	const NOW = new Date();
	const todyOrdr = await models.order.findAndCountAll({
		where: {
			createdAt: { 
			  [Op.gt]: TODAY_START,
			  [Op.lt]: NOW
			},
		  },
		
		
	});

	return todyOrdr;

}
async function dailyOrder(){
	var firstDay = new Date().setHours(0, 0, 0, 0);
	console.log(firstDay,'firstDay');

	var lastDay = new Date().setHours(23, 59, 59, 0);
	var  date = firstDay;
	var dailyOrdrs = [];

	for(var i=1; i<= 7 ; i++){
		
		  console.log('firstDay',firstDay);
		  console.log('lastDay',lastDay);
			var dailyOrdr = await models.order.findAndCountAll({
				where: {
					createdAt: { 
					  [Op.gt]: firstDay,
					  [Op.lt]: lastDay
					},
				  },
			});
			dailyOrdrs.push(dailyOrdr.count);
        	yesterday = date - 1000 * 60 * 60 * 24 * i;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
			firstDay = new Date(yesterday).setHours(0, 0, 0, 0);
			lastDay = new Date(yesterday).setHours(23, 59, 59, 59);
			
		}

	return dailyOrdrs;

}
async function monthsOrder(){

	var date = new Date(), y = date.getFullYear(), m = date.getMonth();
	var limit = m;
     var mnthOrdrs =[];
	var months = [];
	for(var i=0; i<= limit ; i++){
	var firstDay = new Date(y, m, 1).setHours(0, 0, 0, 0);
	var lastDay = new Date(y, m + 1, 0).setHours(23, 59, 59, 0);
		var mnthOrdr = await models.order.findAndCountAll({
			where: {
				createdAt: { 
				  [Op.gt]: firstDay,
				  [Op.lt]: lastDay
				},
			  },
			
			
		});
		mnthOrdrs.push(mnthOrdr.count);
		m--;
	}
	

	return mnthOrdrs;

}


/**
 * Get one orders
 * @param {Number} OrderId unique db OrderId
 */


async function getliveorders(){


	var _order = await models.order.findAll({
		where: {
			paid: {
				[Op.eq]: "true",
			},
		},
		attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
		include: [
			{
				model: models.orderitem,
				attributes: ['name','quantity','storeId', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
				include: {
					model: models.barcodemaster,
					attributes: ['ProductVariant','BarCode'],
				},
			},
			{
				model: models.store,
				attributes: ['store_id', 'name'],
			},
			
			{
				model: models.userprofile,
				attributes: ['name', 'address', 'phone','email'],
			},
			{
				model: models.ordertransaction,
				attributes: ['gateway'],
			},
			{
				model: models.user,
				attributes: ['name', 'phone','address','email','discountavail'],
			},
			
		],
		limit:20,
		order: [ [ 'createdAt', 'DESC' ]],
	});


console.log('orers',_order[0].orderitems);

	return _order;
}





async function getlatestorders(){


	var _order = await models.order.findAll({
		where: {
			paid: {
				[Op.eq]: "true",
			},
		},
		attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
		include: [
			{
				model: models.orderitem,
				attributes: ['name','quantity','storeId', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
				include: {
					model: models.barcodemaster,
					attributes: ['ProductVariant','BarCode'],
				},
			},
			{
				model: models.store,
				attributes: ['store_id', 'name'],
			},
			
			{
				model: models.userprofile,
				attributes: ['name', 'address', 'phone','email'],
			},
			{
				model: models.ordertransaction,
				attributes: ['gateway'],
			},
			{
				model: models.user,
				attributes: ['name', 'phone','address','email','discountavail'],
			},
			
		],
		limit:1,
		order: [ [ 'createdAt', 'DESC' ]],
	});




	return _order;
}
async function getdispatchorders(){


	// var _order = await models.order.findAll({
	// 	where: {
	// 		paid: {
	// 			[Op.eq]: "true",
	// 		},
	// 	},
	// 	attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
	// 	include: [
	// 		{
	// 			model: models.orderitem,
	// 			attributes: ['name','quantity','storeId', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
	// 			include: {
	// 				model: models.barcodemaster,
	// 				attributes: ['ProductVariant','BarCode'],
	// 			},
	// 		},
	// 		{
	// 			model: models.store,
	// 			attributes: ['store_id', 'name'],
	// 		},
			
	// 		{
	// 			model: models.userprofile,
	// 			attributes: ['name', 'address', 'phone','email'],
	// 		},
	// 		{
	// 			model: models.user,
	// 			attributes: ['name', 'phone','address','email','discountavail'],
	// 		},
			
	// 	],
	// 	limit:200,
	// 	order: [ [ 'createdAt', 'DESC' ]],
	// });



	var _order = await models.driverordermap.findAll({
		where: {
			status: {
				[Op.eq]: "DELIVERED",
			},
		},
	
		limit:20,
		order: [ [ 'createdAt', 'DESC' ]],
	});

	var _orderp = await models.driverordermap.findAll({
			where: {
			status: {
				[Op.eq]: "Dispatched",
			},
		},
	
		limit:20,
		order: [ [ 'createdAt', 'DESC' ]],
	});

	return {orderdel:_order,orderpac:_orderp};
}
async function getOneOrders(OrderId) {
	try {
		var invarr = [];
		var _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: OrderId,
				},
			},
			attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				
				{
					model: models.userprofile,
					attributes: ['name', 'address', 'phone','email'],
				},
				{
					model: models.user,
					attributes: ['name', 'phone','address','email','discountavail'],
				},
				
			],
		});
		if(_order.length < 1){
  console.log('bro i never fail');
		 _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: OrderId,
				},
			},
			attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity','storeId', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				
				{
					model: models.user,
					attributes: ['name', 'phone','address','email','discountavail'],
				},
			],
		});

	   }

		const _ordertxn = await models.ordertransaction.findOne({
			where: {
				ns_order_id: {
					[Op.eq]: OrderId,
				},
			},
		
		});

		_order.gateway = _ordertxn.gateway;


		console.log("TXN"+_ordertxn.bill_amount);

		var cgstval = 0;
		var sgstval = 0;
		var igstval = 0;
		var cessval = 0;
		var totalpreval = 0;
		var totald = 0;
		var dvals = 0;
		var finaldiscountprice = 0 ;
		var pretaxvalue = 0
		var finalpretaxvalue = 0;

		var mrp = _order.bill_amount;
		_order.bill_amountsys = _order.bill_amount;
		_order.bill_amountact = _order.obillamount;



		

		if(_ordertxn.bill_amount < mrp){

			_order.totalbillamt = _order.obillamount;

			_order.bill_amount = _order.obillamount;

		}
		

		console.log("VILL"+mrp);

		console.log("DVILL"+_order.obillamount);

		for(i=0;i< _order.orderitems.length;i++){


			var totaltaxper = parseFloat(_order.orderitems[i].tax_inc_cess);
				var one = 1;

				var fr =  one + totaltaxper;

				console.log("fraction"+fr);


			if(_ordertxn.bill_amount < mrp ){


				var dval = parseFloat(_order.orderitems[i].item_price)/parseFloat(mrp);

					var dmul = dval * 100 ;

					dvals = dmul;

					totald = _order.orderitems[i].item_price - dmul;

					finaldiscountprice = totald*_order.orderitems[i].quantity;

					pretaxvalue = parseFloat(totald)/ parseFloat(fr);

					console.log("pretaxvalue"+pretaxvalue);

					 finalpretaxvalue = pretaxvalue * _order.orderitems[i].quantity;

					 console.log("finalpretaxvalue"+finalpretaxvalue);

					_order.orderitems[i].discountprice = finaldiscountprice.toFixed(2);

					_order.orderitems[i].discountval = (dvals * _order.orderitems[i].quantity).toFixed(2);

					_order.orderitems[i].pretaxvalue = finalpretaxvalue;

					//console.log("taxProdDiscount" + discount);
					

					// pretaxvalue = parseFloat(totald)/ parseFloat(fr);

					if(_order.orderitems[i].cgst == 0.03){

						_order.orderitems[i].cgst = 0.025;

					}

					if(_order.orderitems[i].sgst == 0.03){

						_order.orderitems[i].sgst = 0.025;

					}

					_order.orderitems[i].isgst  = 0;

					

					

					


					_order.orderitems[i].cgstamt = (parseFloat(_order.orderitems[i].cgst) * finalpretaxvalue).toFixed(2);
					_order.orderitems[i].sgstamt= (parseFloat(_order.orderitems[i].sgst) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].cessamt = (parseFloat(_order.orderitems[i].cess) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].igstamt = 0;




					 console.log(_order.orderitems[i].cgst);
console.log(_order.orderitems[i].sgst);
console.log(_order.orderitems[i].igstamt);
console.log(_order.orderitems[i].cessamt);
console.log(_order.orderitems[i].pretaxvalue);




			cgstval += parseFloat(_order.orderitems[i].cgstamt);
			sgstval += parseFloat(_order.orderitems[i].sgstamt);
			igstval += parseFloat(_order.orderitems[i].igstamt);
			cessval += parseFloat(_order.orderitems[i].cessamt);
			totalpreval += parseFloat(_order.orderitems[i].pretaxvalue);

			const _inventory = await models.inventory.findAll({
				where: {
					barcode: {
						[Op.eq]: _order.orderitems[i].barcodemaster.BarCode,
					},
				},
				attributes: ['name', 'price', 'qtytotal', 'discountPrice','barcode'],
				
			});


			//console.log(_inventory);

			


			_inventory.forEach( async element => {
//console.log(element.qtytotal);

				if(element.qtytotal == 0 || element.qtytotal == "0" ){

					invarr.push({'name':element.name,'qty':element.qtytotal,'barcode':element.barcode});


				}


				
				})




			}else{

				pretaxvalue = parseFloat(_order.orderitems[i].item_price)/ parseFloat(fr);

				 finalpretaxvalue = pretaxvalue * _order.orderitems[i].quantity;
				

				_order.orderitems[i].discountprice = 0;

					_order.orderitems[i].discountval = 0;

					_order.orderitems[i].pretaxvalue = finalpretaxvalue;


					if(_order.orderitems[i].cgst == 0.03){

						_order.orderitems[i].cgst = 0.025;

					}

					if(_order.orderitems[i].sgst == 0.03){

						_order.orderitems[i].sgst = 0.025;

					}

					_order.orderitems[i].isgst  = 0;
					
					_order.orderitems[i].cgstamt = (parseFloat(_order.orderitems[i].cgst) * finalpretaxvalue).toFixed(2);
					_order.orderitems[i].sgstamt= (parseFloat(_order.orderitems[i].sgst) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].cessamt = (parseFloat(_order.orderitems[i].cess) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].igstamt = 0;

				console.log(_order.orderitems[i].cgstamt);
				console.log(_order.orderitems[i].sgstamt);
				console.log(_order.orderitems[i].igstamt);
				console.log(_order.orderitems[i].cessamt);
				console.log(_order.orderitems[i].pretaxvalue);
				
							cgstval += parseFloat(_order.orderitems[i].cgstamt);
							sgstval += parseFloat(_order.orderitems[i].sgstamt);
							igstval += parseFloat(_order.orderitems[i].igstamt);
							cessval += parseFloat(_order.orderitems[i].cessamt);
							totalpreval += parseFloat(_order.orderitems[i].pretaxvalue);
				
							const _inventory = await models.inventory.findAll({
								where: {
									barcode: {
										[Op.eq]: _order.orderitems[i].barcodemaster.BarCode,
									},
								},
								attributes: ['name', 'price', 'qtytotal', 'discountPrice','barcode'],
								
							});
				
				
							//console.log(_inventory);
				
							
				
				
							_inventory.forEach( async element => {
				//console.log(element.qtytotal);
				
								if(element.qtytotal == 0 || element.qtytotal == "0" ){
				
									invarr.push({'name':element.name,'qty':element.qtytotal,'barcode':element.barcode});
				
				
								}
				
				
								
								})
			}



			



		}

	







		_order.inventory = invarr;
		_order.cgsttot = cgstval.toFixed(2);
		_order.sgsttot = sgstval.toFixed(2);
		_order.igsttot = igstval.toFixed(2);
		_order.cesstot = cessval.toFixed(2);
		_order.ratetot = totalpreval.toFixed(2);
		

		_order.finald = finaldiscountprice.toFixed(2);
		_order.finald = finaldiscountprice.toFixed(2);
		
		if(_order.userprofile){
			
			_order.user.address = _order.userprofile.address;
			
		}


	



		logger.info(`found ${_order.orderitems.length} orderitem(s)`);
		// console.log(_order);
		return _order;
	} catch (err) {
		logger.error(err);
		return [];
	}
}




//New logic

async function getOneOrdersCsv2(OrderId) {
	try {
		var invarr = [];
		const _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: OrderId,
				},
			},
			attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				{
					model: models.user,
					attributes: ['name', 'address', 'phone','email','discountavail'],
				},
			],
		});


		const _ordertxn = await models.ordertransaction.findOne({
			where: {
				ns_order_id: {
					[Op.eq]: OrderId,
				},
			},
		
		});

		var cgstval = 0;
		var sgstval = 0;
		var igstval = 0;
		var cessval = 0;
		var totalpreval = 0;
		var totald = 0;
		var dvals = 0;
		var finaldiscountprice = 0 ;
		var pretaxvalue = 0
		var finalpretaxvalue = 0;

		var mrp = _order.bill_amount;
		_order.withoutDic = _order.bill_amount;
		_order.bill_amountsys = _order.bill_amount;
		_order.bill_amountact = _order.obillamount;

		_order.mrpv = mrp;



		

		if(_ordertxn.bill_amount < mrp){

			_order.totalbillamt = _order.obillamount;

			_order.bill_amount = _order.obillamount;

		}
		

		for(i=0;i< _order.orderitems.length;i++){


			var totaltaxper = parseFloat(_order.orderitems[i].tax_inc_cess);
				var one = 1;

				var fr =  one + totaltaxper;

				


			if(_ordertxn.bill_amount < mrp){


				pretaxvalue = parseFloat(_order.orderitems[i].item_price)/ parseFloat(fr);

				finalpretaxvalue = pretaxvalue * _order.orderitems[i].quantity;
			   

			   _order.orderitems[i].discountprice = 0;

			   

			   _order.orderitems[i].frp = _order.orderitems[i].item_price * _order.orderitems[i].quantity;

				   _order.orderitems[i].discountval = 0;

				   _order.orderitems[i].pretaxvalue = finalpretaxvalue;


				   if(_order.orderitems[i].cgst == 0.03){

					   _order.orderitems[i].cgst = 0.025;

				   }

				   if(_order.orderitems[i].sgst == 0.03){

					   _order.orderitems[i].sgst = 0.025;

				   }

				   _order.orderitems[i].isgst  = 0;
				   
				   _order.orderitems[i].cgstamt = (parseFloat(_order.orderitems[i].cgst) * finalpretaxvalue).toFixed(2);
				   _order.orderitems[i].sgstamt= (parseFloat(_order.orderitems[i].sgst) * finalpretaxvalue).toFixed(2);
			   _order.orderitems[i].cessamt = (parseFloat(_order.orderitems[i].cess) * finalpretaxvalue).toFixed(2);
			   _order.orderitems[i].igstamt = 0;

		   
			   
						   cgstval += parseFloat(_order.orderitems[i].cgstamt);
						   sgstval += parseFloat(_order.orderitems[i].sgstamt);
						   igstval += parseFloat(_order.orderitems[i].igstamt);
						   cessval += parseFloat(_order.orderitems[i].cessamt);
						   totalpreval += parseFloat(_order.orderitems[i].pretaxvalue);
			   
						   const _inventory = await models.inventory.findAll({
							   where: {
								   barcode: {
									   [Op.eq]: _order.orderitems[i].barcodemaster.BarCode,
								   },
							   },
							   attributes: ['name', 'price', 'qtytotal', 'discountPrice','barcode'],
							   
						   });
			   
			   
						   //console.log(_inventory);
			   
						   
			   
			   
						   _inventory.forEach( async element => {
			   //console.log(element.qtytotal);
			   
							   if(element.qtytotal == 0 || element.qtytotal == "0" ){
			   
								   invarr.push({'name':element.name,'qty':element.qtytotal,'barcode':element.barcode});
			   
			   
							   }
			   
			   
							   
							   })




			}



			



		}

	







		_order.inventory = invarr;
		_order.cgsttot = cgstval.toFixed(2);
		_order.sgsttot = sgstval.toFixed(2);
		_order.igsttot = igstval.toFixed(2);
		_order.cesstot = cessval.toFixed(2);
		_order.ratetot = totalpreval.toFixed(2);

		_order.finald = finaldiscountprice.toFixed(2);


	//console,log(_order);



		//logger.info(`found ${_order.orderitems.length} orderitem(s)`);
		return _order;
	} catch (err) {
		logger.error(err);
		return [];
	}
}



async function getOneOrdersCsv(OrderId) {
	try {
		var invarr = [];
		const _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: OrderId,
				},
			},
			attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				{
					model: models.user,
					attributes: ['name', 'address', 'phone','email','discountavail'],
				},
			],
		});


		const _ordertxn = await models.ordertransaction.findOne({
			where: {
				ns_order_id: {
					[Op.eq]: OrderId,
				},
			},
		
		});

		var cgstval = 0;
		var sgstval = 0;
		var igstval = 0;
		var cessval = 0;
		var totalpreval = 0;
		var totald = 0;
		var dvals = 0;
		var finaldiscountprice = 0 ;
		var pretaxvalue = 0
		var finalpretaxvalue = 0;

		var mrp = _order.bill_amount;
		_order.withoutDic = _order.bill_amount;
		_order.bill_amountsys = _order.bill_amount;
		_order.bill_amountact = _order.obillamount;

		_order.mrpv = mrp;



		

		if(_ordertxn.bill_amount < mrp){

			_order.totalbillamt = _order.obillamount;

			_order.bill_amount = _order.obillamount;

		}
		

		for(i=0;i< _order.orderitems.length;i++){

			if(_order.orderitems[i].deleted != true || _order.orderitems[i].deleted != "true" ){

			


			var totaltaxper = parseFloat(_order.orderitems[i].tax_inc_cess);
				var one = 1;

				var fr =  one + totaltaxper;

				


			if(_ordertxn.bill_amount < mrp){


				var dval = parseFloat(_order.orderitems[i].item_price)*parseFloat(0.1);

					var dmul = dval  ;

					dvals = dmul;

					totald = _order.orderitems[i].item_price - dmul;

					finaldiscountprice = totald*_order.orderitems[i].quantity;

					pretaxvalue = parseFloat(totald)/ parseFloat(fr);

					

					 finalpretaxvalue = pretaxvalue * _order.orderitems[i].quantity;

					

					_order.orderitems[i].discountprice = finaldiscountprice.toFixed(2);

					_order.orderitems[i].frp = finaldiscountprice.toFixed(2);

					_order.orderitems[i].discountval = (dvals * _order.orderitems[i].quantity).toFixed(2);

					_order.orderitems[i].pretaxvalue = finalpretaxvalue;

					//console.log("taxProdDiscount" + discount);
					

					// pretaxvalue = parseFloat(totald)/ parseFloat(fr);

					if(_order.orderitems[i].cgst == 0.03){

						_order.orderitems[i].cgst = 0.025;

					}

					if(_order.orderitems[i].sgst == 0.03){

						_order.orderitems[i].sgst = 0.025;

					}

					_order.orderitems[i].isgst  = 0;

					

					

					


					_order.orderitems[i].cgstamt = (parseFloat(_order.orderitems[i].cgst) * finalpretaxvalue).toFixed(2);
					_order.orderitems[i].sgstamt= (parseFloat(_order.orderitems[i].sgst) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].cessamt = (parseFloat(_order.orderitems[i].cess) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].igstamt = 0;




				




			cgstval += parseFloat(_order.orderitems[i].cgstamt);
			sgstval += parseFloat(_order.orderitems[i].sgstamt);
			igstval += parseFloat(_order.orderitems[i].igstamt);
			cessval += parseFloat(_order.orderitems[i].cessamt);
			totalpreval += parseFloat(_order.orderitems[i].pretaxvalue);

			const _inventory = await models.inventory.findAll({
				where: {
					barcode: {
						[Op.eq]: _order.orderitems[i].barcodemaster.BarCode,
					},
				},
				attributes: ['name', 'price', 'qtytotal', 'discountPrice','barcode'],
				
			});


			//console.log(_inventory);

			


			_inventory.forEach( async element => {
//console.log(element.qtytotal);

				if(element.qtytotal == 0 || element.qtytotal == "0" ){

					invarr.push({'name':element.name,'qty':element.qtytotal,'barcode':element.barcode});


				}


				
				})




			}else{

				pretaxvalue = parseFloat(_order.orderitems[i].item_price)/ parseFloat(fr);

				 finalpretaxvalue = pretaxvalue * _order.orderitems[i].quantity;
				

				_order.orderitems[i].discountprice = 0;

				_order.orderitems[i].frp = _order.orderitems[i].item_price * _order.orderitems[i].quantity;

					_order.orderitems[i].discountval = 0;

					_order.orderitems[i].pretaxvalue = finalpretaxvalue;


					if(_order.orderitems[i].cgst == 0.03){

						_order.orderitems[i].cgst = 0.025;

					}

					if(_order.orderitems[i].sgst == 0.03){

						_order.orderitems[i].sgst = 0.025;

					}

					_order.orderitems[i].isgst  = 0;
					
					_order.orderitems[i].cgstamt = (parseFloat(_order.orderitems[i].cgst) * finalpretaxvalue).toFixed(2);
					_order.orderitems[i].sgstamt= (parseFloat(_order.orderitems[i].sgst) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].cessamt = (parseFloat(_order.orderitems[i].cess) * finalpretaxvalue).toFixed(2);
				_order.orderitems[i].igstamt = 0;

			
				
							cgstval += parseFloat(_order.orderitems[i].cgstamt);
							sgstval += parseFloat(_order.orderitems[i].sgstamt);
							igstval += parseFloat(_order.orderitems[i].igstamt);
							cessval += parseFloat(_order.orderitems[i].cessamt);
							totalpreval += parseFloat(_order.orderitems[i].pretaxvalue);
				
							const _inventory = await models.inventory.findAll({
								where: {
									barcode: {
										[Op.eq]: _order.orderitems[i].barcodemaster.BarCode,
									},
								},
								attributes: ['name', 'price', 'qtytotal', 'discountPrice','barcode'],
								
							});
				
				
							//console.log(_inventory);
				
							
				
				
							_inventory.forEach( async element => {
				//console.log(element.qtytotal);
				
								if(element.qtytotal == 0 || element.qtytotal == "0" ){
				
									invarr.push({'name':element.name,'qty':element.qtytotal,'barcode':element.barcode});
				
				
								}
				
				
								
								})
			}



			


		}

		}

	







		_order.inventory = invarr;
		_order.cgsttot = cgstval.toFixed(2);
		_order.sgsttot = sgstval.toFixed(2);
		_order.igsttot = igstval.toFixed(2);
		_order.cesstot = cessval.toFixed(2);
		_order.ratetot = totalpreval.toFixed(2);

		_order.finald = finaldiscountprice.toFixed(2);


	//console,log(_order);



		//logger.info(`found ${_order.orderitems.length} orderitem(s)`);
		return _order;
	} catch (err) {
		logger.error(err);
		return [];
	}
}




async function delteOrders(pid,qty,qtyorg,bill,itembillamt,withoutDic,orderid) {
	try {

		var qtymk = qtyorg - qty;
		var withoutDic1 = parseFloat(withoutDic);
		withoutDic1 = withoutDic1.toFixed(2);
		const totalafterdel = itembillamt * qtyorg;
		var bill1 = parseFloat(bill).toFixed(2);
		withoutDic1 = withoutDic1 - bill1;
		var totalRe =  parseFloat(withoutDic);
		totalRe = totalRe.toFixed(2);		
		var nowFinal = parseFloat(totalRe) - parseFloat(totalafterdel);
		nowFinal = nowFinal.toFixed(2);
		console.log('totalRe',totalRe);
		console.log('bill1',bill1);
		if(totalRe > bill1){
		   console.log('entereing here');
			var decrease_percent = withoutDic1 / totalRe;
			decrease_percent = decrease_percent.toFixed(2);
			console.log(decrease_percent,'decrease_perhere');
			var nowBill = parseFloat(totalRe) - parseFloat(totalafterdel);
			nowBill =  nowBill.toFixed(2);
			 var nowDisc = nowBill * decrease_percent;
			 nowDisc = nowDisc.toFixed(2);
			  nowFinal = parseFloat(nowBill) - parseFloat(nowDisc);
			  nowFinal = nowFinal.toFixed(2);
		}
		
		var rembill = parseFloat(totalRe) - parseFloat(totalafterdel);
		rembill = rembill.toFixed(2);

		console.log('nowFinal',nowFinal);
		console.log('rembill',rembill);

		var cur_data ={bill_amount:rembill,obillamount:nowFinal,quantity:qty};
		var prev_data ={bill_amount:totalRe,obillamount:bill1, quantity:qtyorg};
		var insertData  = {};
		insertData.action = 'update';
		insertData.order_id = orderid;
		insertData.barcodemasterId = pid;
		insertData.cur_data = JSON.stringify(cur_data);
		insertData.prev_data = JSON.stringify(prev_data);
		const _updated1 = await models.order.update({ bill_amount : rembill , obillamount : nowFinal }, {
			where: {
				order_id: {
					[Op.eq]: orderid,
				},
			},
		});		
		const _updated = await models.orderitem.update({ deleted: "true" , quantity : qty }, {
			where: {
				barcodemasterId: {
					[Op.eq]: parseInt(pid),
				},
			},
		});
		const updateLog =  await models.orderupdate.create(insertData);
		return "true";
	} catch (err) {
		logger.error(err);
		return [];
	}
}
async function updateOrders(pid,qty,qtyorg,bill,itembillamt,withoutDic,orderid) {
	try {

		var qtymk = qtyorg - qty;
		var withoutDic1 = parseFloat(withoutDic);
		withoutDic1 = withoutDic1.toFixed(2);
		const totalafterdel = itembillamt * qtymk;
		var bill1 = parseFloat(bill).toFixed(2);
		withoutDic1 = withoutDic1 - bill1;
		var totalRe =  parseFloat(withoutDic);
		totalRe = totalRe.toFixed(2);		
		var nowFinal = parseFloat(totalRe) - parseFloat(totalafterdel);
		nowFinal = nowFinal.toFixed(2);
		console.log('totalRe',totalRe);
		console.log('bill1',bill1);
		if(totalRe > bill1){
		   console.log('entereing here');
			var decrease_percent = withoutDic1 / totalRe;
			decrease_percent = decrease_percent.toFixed(2);
			console.log(decrease_percent,'decrease_perhere');
			var nowBill = parseFloat(totalRe) - parseFloat(totalafterdel);
			nowBill =  nowBill.toFixed(2);
			 var nowDisc = nowBill * decrease_percent;
			 nowDisc = nowDisc.toFixed(2);
			  nowFinal = parseFloat(nowBill) - parseFloat(nowDisc);
			  nowFinal = nowFinal.toFixed(2);
		}
		
		var rembill = parseFloat(totalRe) - parseFloat(totalafterdel);
		rembill = rembill.toFixed(2);

		console.log('nowFinal',nowFinal);
		console.log('rembill',rembill);

	   var cur_data ={bill_amount:rembill,obillamount:nowFinal,quantity:qty};
	   var prev_data ={bill_amount:totalRe,obillamount:bill1, quantity:qtyorg};
	   var insertData  = {};
	   insertData.action = 'update';
	   insertData.order_id = orderid;
	   insertData.barcodemasterId = pid;
	   insertData.cur_data = JSON.stringify(cur_data);
	   insertData.prev_data = JSON.stringify(prev_data);
		const _updated1 = await models.order.update({ bill_amount : rembill , obillamount : nowFinal }, {
			where: {
				order_id: {
					[Op.eq]: orderid,
				},
			},
		});		
		const _updated = await models.orderitem.update({ quantity : qty }, {
			where: {
				barcodemasterId: {
					[Op.eq]: parseInt(pid),
				},
			},
		});
		const updateLog =  await models.orderupdate.create(insertData);
		return "true";
	} catch (err) {
		logger.error(err);
		return [];
	}
}

/**
 * Get orders by storeId
 * @param {Number} storeId store id
 * @param {String} status Order Status
 */
async function getStoreOrders(storeId, status,old) {
	try {
		if (!storeId) {
			// this makes sure that only requests that have storeId are displayed
			// as loading orders across stores makes the page slow
			logger.info('no store id = no orders');
			return [];
		}
		const statusCondition = (!status) ? '' : `AND orders.status = '${status}'`;
		logger.info(`storeid is ${storeId}`);
		console.log('storeId',storeId);
		var data='';
		var additionalq ='';
		var additionald ='';
		if(old =='yes'){
			console.log('storeId IF',storeId);
		 
		 data = await models.sequelize.query(`
		SELECT orders.order_id, orders.status,orders."userprofileId", users.name, users.address, 
		users.phone, barcodemasters."ProductVariant", 
		barcodemasters."BrandName", barcodemasters."Company", 
		barcodemasters."Price", storescans."discountPrice", 
		orderitems.item_price, orderitems.quantity, 
		dense_rank() over (partition by "userId" order by orders."createdAt") as order_rank,
		orders."createdAt"
		FROM orders
		JOIN users
		ON orders."userId" = users.id
		JOIN orderitems
		ON orders.id = orderitems."orderId"
		JOIN barcodemasters
		ON orderitems."barcodemasterId" = barcodemasters.id
		JOIN storescans
		ON barcodemasters."BarCode" = storescans.barcode
		AND orders."storeId" = ${storeId}
		${statusCondition}
		AND orders.paid = true
		ORDER BY orders.order_id, orders."createdAt" DESC, order_rank ASC;`);
	 additionalq = await models.sequelize.query(`SELECT * FROM orders JOIN additionals ON orders.id = additionals."orderId" WHERE orders.paid = true   AND orders."storeId" = ${storeId} ${statusCondition}`); 
	 additionald = additionalq[0];
	      }else{
			data = await models.sequelize.query(`
			SELECT orders.order_id, orders.status,orders."userprofileId", users.name, users.address, 
			users.phone, barcodemasters."ProductVariant", 
			barcodemasters."BrandName", barcodemasters."Company", 
			barcodemasters."Price", storescans."discountPrice", 
			orderitems.item_price, orderitems.quantity, 
			dense_rank() over (partition by "userId" order by orders."createdAt") as order_rank,
			orders."createdAt"
			FROM orders
			JOIN orderstorexrefs
			ON  orders.id = orderstorexrefs."orderId" 
			JOIN users
			ON orders."userId" = users.id
			JOIN orderitems
			ON orders.id = orderitems."orderId"
			JOIN barcodemasters
			ON orderitems."barcodemasterId" = barcodemasters.id
			JOIN storescans
			ON barcodemasters."BarCode" = storescans.barcode
			AND orderstorexrefs."storeId" = ${storeId}
			${statusCondition}
			AND orders.paid = true
			ORDER BY orders.order_id, orders."createdAt" DESC, order_rank ASC;`);
		 additionalq = await models.sequelize.query(`SELECT * FROM orders JOIN orderstorexrefs ON orders.id = orderstorexrefs."orderId" JOIN additionals ON orders.id = additionals."orderId" WHERE orders.paid = true   AND orderstorexrefs."storeId" = ${storeId} ${statusCondition}`);
		 additionald = additionalq[0];	  
		console.log('storeId ELSE',storeId);
		 
	}
		const orders = data[0];
		let orderdiv;
		// console.log(orders);
		var i;
		for (i = 0; i < orders.length; i++) {
		// console.log(orders[i].userprofileId);

		if(orders[i].userprofileId !== null){

			// console.log(orders[i].userprofileId);


			const dataaddress = await models.sequelize.query(`
			SELECT address from userprofiles where id = ${orders[i].userprofileId};`);



			// console.log(dataaddress[0][0].address);

			var pincode = dataaddress[0][0].address.toString().trim().substr(dataaddress[0][0].address.toString().length - 6);

			// console.log(pincode);


			orderdiv += ` <tr>
			<td class="text-center">${parseInt(i) + 1}</td>
			<td class="text-center">${orders[i].status}</td>
			<td class="text-center"><a href="/invoice?orderId=${orders[i].order_id}">${orders[i].order_id}</a></td>
			<td class="text-center">${orders[i].name}</td>
			<td class="text-center">${pincode}</td>
			<td class="text-center">${dataaddress[0][0].address}</td>
			<td class="text-center">${orders[i].phone}</td>
			<td class="text-center">${orders[i].ProductVariant}</td>
			<td class="text-center">${orders[i].quantity} </td>
			<td class="text-center">${orders[i].item_price} </td>
			<td class="text-center">${orders[i].item_price * orders[i].quantity }</td>
			<td class="text-center">${orders[i].BrandName}</td>
			<td class="text-center">${orders[i].Company} </td>
			<td class="text-center">${orders[i].createdAt}</td>
			<td class="text-center">${orders[i].order_rank}</td>
		</tr>`;
		}else{

			var pincode = orders[i].address.toString().trim().substr(orders[i].address.toString().length - 6);


			orderdiv += ` <tr>
			<td class="text-center">${parseInt(i) + 1}</td>
			<td class="text-center">${orders[i].status}</td>
			<td class="text-center"><a href="/invoice?orderId=${orders[i].order_id}">${orders[i].order_id}</a></td>
			<td class="text-center">${orders[i].name}</td>
			<td class="text-center">${pincode}</td>
			<td class="text-center">${orders[i].address}</td>
			<td class="text-center">${orders[i].phone}</td>
			<td class="text-center">${orders[i].ProductVariant}</td>
			<td class="text-center">${orders[i].quantity} </td>
			<td class="text-center">${orders[i].item_price} </td>
			<td class="text-center">${orders[i].item_price * orders[i].quantity }</td>
			<td class="text-center">${orders[i].BrandName}</td>
			<td class="text-center">${orders[i].Company} </td>
			<td class="text-center">${orders[i].createdAt}</td>
			<td class="text-center">${orders[i].order_rank}</td>
		</tr>`;
		}
		}


		
		logger.info(`found ${orders.length} order(s)`);
		return { orders, additional: additionald,orderdiv:orderdiv };
	} catch (err) {
		logger.error(err);
		return [];
	}
}


/**
 * This function updates the order, orderId's status
 * to `status`
 * @param {String} orderId Order Id
 * @param {String} status Order Status
 * @param {Boolean} paid Order payment Status
 */
async function updateOrderStatus(orderId, status, paid,person) {
	try {


		if(!person){


			const _updated = await models.order.update({ status: status, paid: paid }, {
				where: {
					order_id: {
						[Op.eq]: orderId,
					},
				},
			});


		}else{
			const _updated = await models.order.update({ status: status, paid: paid ,person:person }, {
				where: {
					order_id: {
						[Op.eq]: orderId,
					},
				},
			});
		}
	
		//logger.info(`updated ${_updated[0]} row(s) [${orderId}, ${status}]`);
		return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}



async function createOrderQueue(orderId,location) {
	try {

		var _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: orderId,
				},
			},
			attributes: ['order_id', 'bill_amount', 'status', 'paid','id','createdAt','obillamount','discount'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price','barcodemasterId','hsncode','tax_inc_cess','cgst','sgst','cess','deleted','cgstamt','sgstamt','igst','igstamt','cessamt','tax_inc_cess_amt','pretaxvalue','discountprice','discountval'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode','ProductType'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				
				{
					model: models.userprofile,
					attributes: ['name', 'address', 'phone','email'],
				},
				{
					model: models.user,
					attributes: ['name', 'phone','address','email','discountavail'],
				},
				
			],
		});

		


		if(_order.userprofile){
			
			_order.user.address = _order.userprofile.address;
			
		}

		var categoryarr = [];

		var cat = "";

		var rnk = "";


		for(i=0;i< _order.orderitems.length;i++){

			var prodtype = _order.orderitems[i].barcodemaster.ProductType;

			categoryarr.push(prodtype);




		}

		var n = categoryarr.includes("Frozen Ready to Cook");

		if(n == true){
			cat = "Frozen"

			rnk = 1;



		}else{
			cat = "Other"
			rnk = 5;
		}




		var orderId = orderId;

		var address = _order.user.address;

		var phone  = _order.user.phone;

		var category = cat;

		var cname = _order.user.name;

		var paymentmode = "prepaid";

		var billamt = _order.obillamount;

		var odate = _order.createdAt.toString();
		



		var rank = rnk;


		const _orderqueue = await models.orderqueue.create({
			orderId: orderId,
			address: address,
			phone: phone,
			category: category,
			rank: rank,
			storeid:location,
			customername:cname,
			paymentmode:paymentmode,
			billamt:billamt,
			ordercreationdate: odate,
		});
		

		



		


		

		
		
		//console.log(_order.orderitems);

		
		//return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}



async function updateOrderInventory(orderId, status, paid,locationid) {
	try {

		const _order = await models.order.findOne({
			where: {
				order_id: {
					[Op.eq]: orderId,
				},
			},
			attributes: ['id','order_id', 'bill_amount', 'status', 'paid'],
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant','BarCode'],
					},
				},
				{
					model: models.store,
					attributes: ['store_id', 'name'],
				},
				{
					model: models.user,
					attributes: ['name', 'address', 'phone'],
				},
			],
		});
		
		//console.log(_order.orderitems);

		_order.orderitems.forEach( async element => {
			// console.log(element.quantity);
			// console.log(element.barcodemaster.BarCode);




			const _inventory = await models.storescan.findOne({
				where: {
					barcode: {
						[Op.eq]: element.barcodemaster.BarCode,
					},
					box_id:{
						[Op.eq]:locationid,
					},
				},
				
			});
			// get Storage info ******* *** storage things
			const _storage = await models.storage.findOne({
				where: {
					id: {
						[Op.eq]: _inventory.storage_id,
					},
					locid:{
						[Op.eq]:locationid,
					},
				},
				
			});

			//*** storage things



if (!_inventory) {
			

}else if(parseInt(_inventory.qtyavailiable) > 0){
	var qty = parseInt(_inventory.qtyavailiable);
			
			var prodqty = parseInt(element.quantity);

			var qtyav =  qty - prodqty;

			// calculating occupancy *** storage things
			try {

				var storage_occupancy = _storage.storage_occupancy;
				
			} catch (error) {
				
			}
			
			storage_occupancy =	parseFloat(storage_occupancy);
			var storage_consume_unit  = _inventory.storage_consume_unit;
			storage_consume_unit = storage_consume_unit * prodqty;
			storage_occupancy = storage_occupancy - storage_consume_unit;
			storage_occupancy = storage_occupancy.toFixed(2);
			if(storage_occupancy < 0){
				storage_occupancy = 0;
			}
			var updateData = {storage_occupancy: storage_occupancy};


			var updatedStorage = await models.storage.update(
				updateData,
				 { where: { 
					 id: _inventory.storage_id,
				  } 
				 }
			   )
	   /// *** storage things
	   
			console.log(qtyav);



			const _updated = await models.storescan.update({ qtyavailiable: qtyav }, {
				where: {
					barcode: {
						[Op.eq]: element.barcodemaster.BarCode,
					},
					box_id:{
						[Op.eq]:locationid,
					},
				},
			});
}else{
	
}
		  });
		//return true;
	} catch (err) {
		logger.error(err);
		return false;
	}
}

/**
 * Returns orders by createdAt order
 */
async function getOrderByOrder() {
	try {
		const _orders = await models.order.findAll({
			where: {
				paid: {
					[Op.eq]: true,
				},
				createdAt: {
					[Op.gt]: models.sequelize.cast('2020-06-02 00:00:00', 'timestamp'),
					[Op.lt]: models.sequelize.cast('2020-06-02 18:00:00', 'timestamp'),
				},
			},
			include: [
				{
					model: models.orderitem,
					attributes: ['quantity', 'item_price'],
					include: {
						model: models.barcodemaster,
						attributes: ['ProductVariant'],
					},
				},
			],
		});
		logger.info(`found ${_orders.length} row(s)`);
		return _orders;
	} catch (err) {
		logger.error(err);
		return [];
	}
}


module.exports = {
	getNumberOfOrderByDay,
	dashboardData,
	getAllOrders,
	getAllUnpaidOrders,
	getOneOrders,
	getStoreOrders,
	updateOrderStatus,
	getOrderByOrder,
	updateOrderInventory,
	delteOrders,
	updateOrders,
	todaysOrder,
	monthsOrder,
	dailyOrder,
	getallpackingpersons,
	getOneOrdersCsv,
	getAllOrderitems,
	getOneOrdersCsv2,
	getliveorders,
	getdispatchorders,
	getlatestorders,
	createOrderQueue,
};
