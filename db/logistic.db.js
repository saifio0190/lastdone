/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
const models        = require('../models');
const Op            = require('sequelize').Op;
const log4js        = require('log4js');
const { compareSync } = require('bcryptjs');
const logger        = log4js.getLogger('db/order.db.js');
logger.level        = log4js.levels.INFO.levelStr;

/**
 * This function returns number of orders by
 * day on near.store
 * @return {JSON} payload
 */
async function getNumberOfOrderByDay() {

}


/**
 * this function fetches dashboard info
 * data
 * @return {JSON} payload
 */


/**
 * Get all orders
 * @param {Number} storeId unique db storeId
 * @param {String} status Order status
 * @param {String} paidStatus Order's paid status (pending / paid)
 */

      async function getDownload(from, to){
        
        const query = `SELECT * FROM logisticsitems WHERE "createdAt" > '${from}' AND "createdAt" < '${to}'`;
        
       data = await models.sequelize.query(query);
       return data[0];
      }
      async function downloadById(order_id_Search){
        
        const query = `SELECT * FROM logisticsitems WHERE "order_id" = '${order_id_Search}' ORDER BY id DESC`;
        
       data = await models.sequelize.query(query);
       return data[0];
      }
      

     async function getAlllogistics(){

const models        = require('../models');
const query = `SELECT * FROM orderdeliveries
             WHERE task_id is NOT NULL AND "state" !='NA' ORDER BY id DESC`;
      data = await models.sequelize.query(query);
      console.log(data[0][0]);
        return data[0];
      }

    








module.exports = {
    getAlllogistics,
    getDownload,
    downloadById,
};
