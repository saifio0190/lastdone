/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
const models        = require('../models');
const Op            = require('sequelize').Op;
const bcrypt        = require('bcryptjs');
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/admin.db.js');
logger.level        = log4js.levels.INFO.levelStr;


/**
 * This function gets the user
 * based on email and password provided
 * @param {String} email User Email
 */
async function getUser(email) {
	try {
		let _user = await models.admin.findOne({
			where: {
				email: {
					[Op.eq]: email,
				},
			},
		});
		if (!_user) {
			logger.error(`unable to find user with email ${email}`);
			return;
		}
		logger.info('found one user');
		return _user;
	} catch (err) {
		logger.error(err);
		return null;
	}
}

/**
 * This function gets the user
 * based on userId provided
 * @param {Number} Id User Id
 */
async function getUserById(Id) {
	try {
		let _user = await models.admin.findOne({
			where: {
				id: {
					[Op.eq]: Id,
				},
			},
		});
		if (!_user) {
			logger.error(`unable to find user with id ${id}`);
			return;
		}
		logger.info('found one user');
		return _user;
	} catch (err) {
		logger.error(err);
		return null;
	}
}

/**
 * This function authenticates the user
 * based on email and password provided
 * @param {String} password User Password
 * @param {String} hash User password hash
 * @return {Boolean} indication whether the
 * password was correct or not
 */
async function authenticateUser(password, hash) {
	try {
		logger.info(hash, password);
		return bcrypt.compareSync(password, hash);
	} catch (err) {
		logger.error(err);
		return false;
	}
}


/**
 * Register a new user with the platform
 * @param {String} name User's name
 * @param {String} email User's email
 * @param {String} password plaintext password
 */
async function registerNewUser(name, email, password) {
	try {
		const salt = bcrypt.genSaltSync(10);
		const passwordHash = bcrypt.hashSync(password, salt);
		let _user = await models.admin.findOne({
			where: {
				email: {
					[Op.eq]: email,
				},
			},
		});
		if (_user) {
			logger.info(`user with email exists`);
			return false;
		}
		const newUser = await models.admin.create({
			name: name,
			email: email,
			password: passwordHash,
			active: false,
		});
		if (newUser) {
			logger.info(`inserted user with email ${email}`);
			return true;
		}
		return false;
	} catch (err) {
		logger.error(err);
		return false;
	}
}

module.exports = {
	getUser,
	getUserById,
	registerNewUser,
	authenticateUser,
};
