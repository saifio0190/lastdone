/* eslint-disable prefer-const */
/* eslint-disable max-len */
/* eslint-disable no-multi-spaces */
/* eslint-disable object-curly-spacing */
const models        = require('../models');
const sequelize     = require('sequelize');
const QueryTypes    = sequelize.QueryTypes;
const Op            = sequelize.Op;
const fs        = require('fs');
const log4js        = require('log4js');
const logger        = log4js.getLogger('db/product.db.js');
logger.level        = log4js.levels.INFO.levelStr;




/**
 * This function check if a given barcode is already
 * being used
 *
 * @param {String} barcode product barcode to check
 * @return {Boolean} indication whether the barcode exists or not
 * @retval true barcode does not exists and can be assigned to new product
 * @retval false barcode is in use, do not assign to another product
 */
async function barcodeCheck(barcode,location) {
	try {
		const product = await models.barcodemaster.findOne({
			where: {
				BarCode: {
					[Op.eq]: barcode,
				},
			},
		});
		const product1 = await models.storescan.findOne({
			where: {
				barcode: {
					[Op.eq]: barcode,
				},
				box_id: {
					[Op.eq]: location,
				},
			},
		});
	
			if(!product1){
			logger.info(`product does not exist with ${barcode}`);
			return true;
	
		}
		logger.info(`product exists with ${barcode}`);
		return false;
	} catch (err) {
		logger.error(err);
		return false;
	}
}


async function updateStorage( id,updateData){
	var result = {status:0,data:'failed'};
	console.log(updateData,'updateData');
	console.log(id,'barcoididde');
	var updated =  models.storage.update(
		updateData,
		 { where: { 
			 id: id,
		  } 
		 }
	   )
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
	
	  return updated;
}
async function updateBarcodeProductByBarcode(updateData, barcode){
	var result = {status:0,data:'failed'};
	console.log(updateData,'updateDataBarcpde');
	console.log(barcode,'barcode');
	var updated =  models.barcodemaster.update(
		updateData,
		 { where: { 
			 BarCode: barcode,
		  } 
		 }
	   )
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
	
	  return updated;
}


async function getstoragecapacityBrand(storeId,brand){
	var locid='';
	
 if(storeId == '175'){
    locid ='e0512496NSKI';
 }
 if(storeId == '239'){
	locid ='e0512496BANR';
}

var wherePro ={
	storage_id: {
		[Op.ne]: null,
	},
};
var whereRefr = {
	storage_type: {[Op.eq]: "refrigerator",	}
  };
  var whereFridge = {
	storage_type: {[Op.eq]: "freezer",	}
  };
  var whereRegular = {
	storage_type: {[Op.eq]: "regular",	},
  }
  var whereBrand = {

	brand: {[Op.ne]: null,},
	storage_id: {[Op.ne]: null,},
  }


  if(brand){


	console.log('here in brand')
if(locid){

	console.log('here inside location too');
	wherePro ={
		storage_id: {[Op.ne]: null,},
		storage_type: {[Op.ne]: null,},
		box_id: {[Op.eq]: locid,},
		brand: {[Op.eq]: brand,},
	};
}else{
	console.log('here inside brand only');
	wherePro ={
		storage_id: {[Op.ne]: null,},
		storage_type: {[Op.ne]: null,},
		brand: {[Op.eq]: brand,},
	};
}
}

if(locid){

	if(brand){

		wherePro ={
			storage_id: {[Op.ne]: null,},
			storage_type: {[Op.ne]: null,},
			box_id: {[Op.eq]: locid,},
			brand: {[Op.eq]: brand,},
		};	
	}else{
		wherePro ={
			storage_id: {[Op.ne]: null,},
			storage_type: {[Op.ne]: null,},
			box_id: {[Op.eq]: locid,},
		};
	}


	whereRefr = {
		storage_type: {[Op.eq]: "refrigerator",	},
		locid: {[Op.eq]: locid,	}
	  };
	  whereFridge = {
		storage_type: {[Op.eq]: "freezer",	},
		locid: {[Op.eq]: locid,	}
	  };
	  whereRegular = {
		storage_type: {[Op.eq]: "regular",	},
		locid: {[Op.eq]: locid,	}
	  };
	  whereBrand = {
		brand: {[Op.ne]: null,},
		storage_id: {[Op.ne]: null,},
		box_id: {[Op.eq]: locid,}
	  };
}


const brands = await models.storescan.findAll({
	attributes: [
		[sequelize.fn('DISTINCT', sequelize.col('brand')), 'brand'],
	],
	where: whereBrand,
	order: ['brand'],
});
const products = await models.storescan.findAll({
	where: wherePro,
	order: [ [ 'id', 'DESC' ]],
});

	const refrigeratorstorage = await models.storage.findAll({
		where: whereRefr,
	});


	const fridgestorage = await models.storage.findAll({
		where:whereFridge,
	});


	const regularstorage = await models.storage.findAll({
		where:whereRegular,
	});

	var refrigeratoravailiablespace = 0 ;
	var refrigeratoroccupiedspace = 0 ;

	for(i=0;i<refrigeratorstorage.length;i++){

		refrigeratoravailiablespace += parseFloat(refrigeratorstorage[i].storage_capacity) ;
		refrigeratoroccupiedspace +=  parseFloat(refrigeratorstorage[i].storage_occupancy) ;
	}


	var fridgeavailiablespace = 0 ;
	var fridgeoccupiedspace = 0 ;

	for(i=0;i<fridgestorage.length;i++){

		fridgeavailiablespace += parseFloat(fridgestorage[i].storage_capacity) ;
		fridgeoccupiedspace +=  parseFloat(fridgestorage[i].storage_occupancy) ;
	}


	var regularavailiablespace = 0 ;
	var regularoccupiedspace = 0 ;

	for(i=0;i<regularstorage.length;i++){

		regularavailiablespace += parseFloat(regularstorage[i].storage_capacity) ;
		regularoccupiedspace +=  parseFloat(regularstorage[i].storage_occupancy) ;
	}


	return {refrigeratoravailiablespace,refrigeratoroccupiedspace,fridgeavailiablespace,fridgeoccupiedspace,regularavailiablespace,regularoccupiedspace,products:products,brands:brands};



}
async function getstoragecapacity(storeId){
	var locid='';
 if(storeId == '175'){
    locid ='e0512496NSKI';
 }
 if(storeId == '239'){
	locid ='e0512496BANR';
}

var wherePro ={
	storage_id: {
		[Op.ne]: null,
	},
};
var whereRefr = {
	storage_type: {[Op.eq]: "refrigerator",	}
  };
  var whereFridge = {
	storage_type: {[Op.eq]: "freezer",	}
  };
  var whereRegular = {
	storage_type: {[Op.eq]: "regular",	},
  }
if(locid){

	wherePro ={
		storage_id: {[Op.ne]: null,},
		storage_type: {[Op.ne]: null,},
		box_id: {[Op.eq]: locid,}
	};
	whereRefr = {
		storage_type: {[Op.eq]: "refrigerator",	},
		locid: {[Op.eq]: locid,	}
	  };
	  whereFridge = {
		storage_type: {[Op.eq]: "freezer",	},
		locid: {[Op.eq]: locid,	}
	  };
	  whereRegular = {
		storage_type: {[Op.eq]: "regular",	},
		locid: {[Op.eq]: locid,	}
	  };
}
const products = await models.storescan.findAll({
	where: wherePro,
	order: [ [ 'id', 'DESC' ]],
});

	const refrigeratorstorage = await models.storage.findAll({
		where: whereRefr,
	});


	const fridgestorage = await models.storage.findAll({
		where:whereFridge,
	});


	const regularstorage = await models.storage.findAll({
		where:whereRegular,
	});

	var refrigeratoravailiablespace = 0 ;
	var refrigeratoroccupiedspace = 0 ;

	for(i=0;i<refrigeratorstorage.length;i++){

		refrigeratoravailiablespace += parseFloat(refrigeratorstorage[i].storage_capacity) ;
		refrigeratoroccupiedspace +=  parseFloat(refrigeratorstorage[i].storage_occupancy) ;
	}


	var fridgeavailiablespace = 0 ;
	var fridgeoccupiedspace = 0 ;

	for(i=0;i<fridgestorage.length;i++){

		fridgeavailiablespace += parseFloat(fridgestorage[i].storage_capacity) ;
		fridgeoccupiedspace +=  parseFloat(fridgestorage[i].storage_occupancy) ;
	}


	var regularavailiablespace = 0 ;
	var regularoccupiedspace = 0 ;

	for(i=0;i<regularstorage.length;i++){

		regularavailiablespace += parseFloat(regularstorage[i].storage_capacity) ;
		regularoccupiedspace +=  parseFloat(regularstorage[i].storage_occupancy) ;
	}


	return {refrigeratoravailiablespace,refrigeratoroccupiedspace,fridgeavailiablespace,fridgeoccupiedspace,regularavailiablespace,regularoccupiedspace,products:products};



}
async function updateStoreProductByBarcode(updateData, barcode, whlocation,barcodeData){
	var result = {status:0,data:'failed'};
	   
		var insertData  = {};
		insertData.action = 'update';
		insertData.barcode = barcode;
		insertData.box_id = whlocation;
		insertData.cur_data = JSON.stringify(updateData);
		insertData.prev_data = JSON.stringify(barcodeData);
		// insertData.cur_data = 'some';
		// insertData.prev_data = 'some';
	var updated =  models.storescan.update(
		updateData,
		 { where: { 
			 barcode: barcode,
			 box_id: whlocation,
		  } 
		 }
	   )
	   .then(data => {
		 result = {status:1,data:'success'};
		 var data = "updateData -,"+JSON.stringify(updateData)+","+new Date();
          writePromoData(data);
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		 
  const updateLog =  await models.inventoryupdate.create(insertData);
		
	  return updated;
}
async function addBarcodeProductByBarcode(insertData){
	var result = {status:0,data:'failed'};
	
var maxId =  await	models.barcodemaster.findOne({
		order: [ [ 'id', 'DESC' ]],
	});
	console.log('maxId',maxId.dataValues.id)
	insertData.id =	maxId.dataValues.id+1;
	var updated =  models.barcodemaster.create(insertData)
	   .then(data => {
		 result = {status:1,data:'success'};
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
		
	  return updated;
}
async function getWarehouses(){
	// var wouseareH = await models.warehouse.findAll({
	// 	order: [ [ 'id', 'DESC' ]],
	// });
	// return wouseareH.dataValues;
}
function writePromoData(lineData){
	var result = {status:0,data:'failed'};
	
	var promo = new Promise((resolve, reject) => {
		
	const stream = fs.createWriteStream('./addUpdatelogInventory.csv',{flags:'a'});
	stream.write(lineData +"\r\n ",function(){
		result = {status:1,data:'success'};
		
	return resolve(result);
	})
	});
	return promo;
	}

async function getAllPros(){

}
async function addStoreProductByBarcode(insertData1){
	var result = {status:0,data:'failed'};

	var maxId = await models.storescan.findOne({
		order: [ [ 'id', 'DESC' ]],
	});
    	var insertData  = {};
		insertData.action = 'insert';
		insertData.barcode = insertData1.barcode;
		insertData.box_id = insertData1.box_id;
		insertData.cur_data = JSON.stringify(insertData1);
		insertData.prev_data = JSON.stringify(insertData1);
	console.log('maxId',maxId.dataValues.id)
	insertData1.id =	maxId.dataValues.id+1;
	var updated =  models.storescan.create(insertData1)
	   .then(data => {
		 result = {status:1,data:'success'};
		 var data = "insertData -,"+JSON.stringify(insertData1)+","+new Date();
		 writePromoData(data);
		  return result;
		  
		 })
		 .catch(err => {
			 result = {status:0,data:'failed'};
			 return result;
		 });
		
		 const updateLog =  await models.inventoryupdate.create(insertData);
	  return updated;
}

async function getProductByBarcode(queryData){

	var result ={status:0, data:"No data found",data1:"no data"};

             const query = `SELECT barcodemasters.id, barcodemasters."ProductVariant", barcodemasters."ProductGroup", barcodemasters."ProductType", barcodemasters."Company", barcodemasters."BrandName", barcodemasters.hsncode, barcodemasters.coi, barcodemasters.tax_inc_cess, barcodemasters.cgst, barcodemasters.sgst, barcodemasters.cess, barcodemasters.igst, barcodemasters.igstamt, barcodemasters.description, barcodemasters.expiry, barcodemasters.cess, barcodemasters.url, barcodemasters."BarCode", barcodemasters."ProductCategory", storescans.sku, storescans.qtyavailiable, storescans.visible, storescans.name, storescans."displayName", storescans.group, storescans.category, storescans.barcode, storescans.sku, storescans.type, storescans.url AS sturl, storescans.box_id, storescans.count, storescans.price, storescans."discountPrice", storescans.newproduct, storescans.hsncode AS sthsncode, storescans.tax_inc_cess AS sttax_inc_cess, storescans.coo, storescans.igst AS stigst, storescans.sgst AS stsgst, storescans.cess AS stcess, storescans.cgst AS stcgst, storescans.company, storescans.igstamt AS stigstamt, storescans.product_type, storescans.description AS stdescription, storescans.brand,storescans.storage_consumed, storescans.storage_consume_unit, storescans.storage_type, storescans.storage_id, storescans.expiry AS stexpiry, storages.storage_name, storages.storage_occupancy, storages.storage_capacity FROM barcodemasters JOIN storescans ON barcodemasters."BarCode" = storescans.barcode 
            JOIN storages ON storescans.storage_id = storages.id WHERE barcodemasters."BarCode"='${queryData.barcode}' AND storescans.box_id='${queryData.whlocation}' ORDER BY barcodemasters.id DESC LIMIT 1`;
			 data = await models.sequelize.query(query);
			 
			 const query1 = `SELECT id,storage_name, storage_type, storage_capacity, storage_occupancy, locid from storages
             WHERE locid='${queryData.whlocation}' ORDER BY id DESC`;
			 data1 = await models.sequelize.query(query1);
			 
			 
			 if(data1){
			 result.data1=data1[0];
			}
             
           if(data[0].length > 0){
			   
			result.status=1;
			result.data=data[0];
		
		   }
		   console.log('data[0]',data[0]);
            return result;

}
async function getInventoryByBarcode(queryData){

	var result ={status:0, data:"No data found"};

             const query = `SELECT storescans.sku, storescans.qtyavailiable, storescans.visible, storescans.name, storescans."displayName", storescans.group, storescans.category, storescans.barcode, storescans.sku, storescans.type, storescans.url AS sturl, storescans.box_id, storescans.count, storescans.price, storescans."discountPrice", storescans.newproduct, storescans.hsncode AS sthsncode, storescans.tax_inc_cess AS sttax_inc_cess, storescans.coo, storescans.igst AS stigst, storescans.sgst AS stsgst, storescans.cess AS stcess, storescans.cgst AS stcgst, storescans.company, storescans.igstamt AS stigstamt, storescans.product_type, storescans.description AS stdescription, storescans.brand, storescans.expiry AS stexpiry FROM barcodemasters JOIN storescans ON barcodemasters."BarCode" = storescans.barcode
             WHERE barcodemasters."BarCode"='${queryData.barcode}' GROUP BY barcodemasters.id, storescans.sku, storescans.qtyavailiable, storescans.visible, storescans.visible, storescans.name, storescans."displayName", storescans.group, storescans.category, storescans.barcode, storescans.sku, storescans.type, storescans.url, storescans.box_id, storescans.count, storescans.price, storescans."discountPrice", storescans.newproduct, storescans.hsncode, storescans.tax_inc_cess, storescans.coo, storescans.igst, storescans.sgst, storescans.cess, storescans.cgst, storescans.company, storescans.igstamt, storescans.product_type, storescans.description, storescans.brand, storescans.expiry ORDER BY barcodemasters.id DESC`;
             data = await models.sequelize.query(query);
             
           if(data[0].length > 0){
			result.status=1;
			result.data=data[0];
           }
            return result;

}



module.exports = {
	barcodeCheck,
	getWarehouses,
	getProductByBarcode,
	updateStorage,
	getInventoryByBarcode,
	addBarcodeProductByBarcode,
	addStoreProductByBarcode,
	updateStoreProductByBarcode,
	updateBarcodeProductByBarcode,
	getstoragecapacity,
	getstoragecapacityBrand,

};

