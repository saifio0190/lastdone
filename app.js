/* eslint-disable object-curly-spacing */
/* eslint-disable no-multi-spaces */
const express       = require('express');
const commander     = require('commander');
const path          = require('path');
const bodyParser    = require('body-parser');
const handlebars    = require('express-handlebars');
const session       = require('express-session');
const cookieParser  = require('cookie-parser');
const passport      = require('passport');
const flash         = require('connect-flash');
const env           = require('dotenv');
const log4js        = require('log4js');
const Handlebars    = require('handlebars');
const allowInsecure = require('@handlebars/allow-prototype-access');
const logger        = log4js.getLogger('app.js');
logger.level        = log4js.levels.INFO.levelStr;

commander.option('--env <ENV>', 'Environment to use for app', 'development');
commander.parse(process.argv);

if (commander.env === 'production') {
	env.config({ path: path.join(__dirname, 'env', 'production.env')});
} else {
	env.config({ path: path.join(__dirname, 'env', 'development.env')});
}

logger.info(`using env: ${commander.env}`);

const port      = process.env.PORT || 1134;
const models    = require('./models');
const app       = express();
const routes    = require('./routes');

app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', handlebars({
	handlebars: allowInsecure.allowInsecurePrototypeAccess(Handlebars),
	defaultLayout: 'base',
	partialsDir: path.join(__dirname, 'views', 'partials'),
	helpers: {
		inc: function(value, option) {
			return parseInt(value) + 1;
		},
		ints: function(value) {
			return parseInt(value);
		},
		eqt: (lvalue, options) => {
			if (lvalue == "true") { 
				return options.fn(this);
			}
			return options.inverse(this);
		},
		slect1:function(selected, options) {
			return options.fn(this).replace(
				new RegExp(' value=\"' + selected + '\"'),
				'$& selected="selected"');

		},
		datecon:function (date) {

			
			var todayTime = new Date(date);
			
			var month = todayTime . getMonth()+1;
			var day = todayTime . getDate();
			var year = todayTime . getFullYear();
			return year + "/" + month + "/" + day;
			},
		localtime: function(value, option) {
			return value.toLocaleString('en-US', { timeZone: 'asia/kolkata' });
		},
		compare:function(val,options){
			if(val == "0" || val <= 0){
				 return options.fn(this);
			}
			return options.inverse(this);


		},
		datedifference:function(startdate){

			var today = new Date();
var Christmas = new Date(startdate);
var diffMs = (today - Christmas);
// console.log(diffMs);

//var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);


var diffMins =(today.getTime() - Christmas.getTime()) / 100000;

return Math.abs(Math.round(diffMins));

		},
		datedifference1:function(startdate,enddate){

			var today = new Date(startdate);
var Christmas = new Date(enddate);
var diffMs = (today - Christmas);
// console.log(diffMs);

var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);

return diffMins;

		},
		datedifferencedays:function(startdate){

			var today = new Date();
var Christmas = new Date(startdate);

const diffTime = Math.abs(today - Christmas);
const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
// console.log(diffMs);


return diffDays;

		},
		compare1:function(val,options){
			if(val < 0){
				 return options.fn(this);
			}
			return options.inverse(this);


		},
		select: function(value, options) {
			//console.log(value);
			return options.fn(this).split('\n').map(function(v) {
				const t = 'value="' + value + '"';
				// eslint-disable-next-line max-len
				return ! RegExp(t).test(v) ? v : v.replace(t, t + ' selected="selected"');
			}).join('\n');
		},
		round:function(value) {
			console.log(parseInt(value));
			return parseFloat(value).toFixed(2);
			},
		math: function(lvalue, operator, rvalue, options) {
			lvalue = parseFloat(lvalue);
			rvalue = parseFloat(rvalue);

			return {
				'+': lvalue + rvalue,
				'-': lvalue - rvalue,
				'*': lvalue * rvalue,
				'/': lvalue / rvalue,
				'%': lvalue % rvalue,
			}[operator];
		},
		mathgst: function(lvalue, operator, rvalue, options) {
			lvalue = parseFloat(lvalue);
			rvalue = parseFloat(rvalue);

			return {
				'+': lvalue + rvalue,
				'-': lvalue - rvalue,
				'*': parseFloat(lvalue * rvalue).toFixed(1),
				'/': lvalue / rvalue,
				'%': lvalue % rvalue,
			}[operator];
		},
		month:function (value) {
			var d = new Date();
			var n = d.getMonth();
			return n;
		  },
		  
		largeNum: function(value, option) {
			return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
		},
		ifequals: function(arg1, arg2, options) {
			return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
		},
		ifnequals: function(arg1,options) {
			return (arg1 != 0) ? options.fn(this) : options.inverse(this);
		},
		gt: function(a, b ){
			a= parseInt(a);
			var next =  arguments[arguments.length-1];
			return (a >= b) ? next.fn(this) : next.inverse(this);
		},
		truncate: function(value, option) {
			return value.toString().substring(
				0,
				// eslint-disable-next-line comma-dangle
				(value.toString().length < 20 ? value.toString().length : 20)
			);
		},
		words:function convertNumberToWords(amount) {
			var words = new Array();
			words[0] = '';
			words[1] = 'One';
			words[2] = 'Two';
			words[3] = 'Three';
			words[4] = 'Four';
			words[5] = 'Five';
			words[6] = 'Six';
			words[7] = 'Seven';
			words[8] = 'Eight';
			words[9] = 'Nine';
			words[10] = 'Ten';
			words[11] = 'Eleven';
			words[12] = 'Twelve';
			words[13] = 'Thirteen';
			words[14] = 'Fourteen';
			words[15] = 'Fifteen';
			words[16] = 'Sixteen';
			words[17] = 'Seventeen';
			words[18] = 'Eighteen';
			words[19] = 'Nineteen';
			words[20] = 'Twenty';
			words[30] = 'Thirty';
			words[40] = 'Forty';
			words[50] = 'Fifty';
			words[60] = 'Sixty';
			words[70] = 'Seventy';
			words[80] = 'Eighty';
			words[90] = 'Ninety';
			amount = amount.toString();
			var atemp = amount.split(".");
			var number = atemp[0].split(",").join("");
			var n_length = number.length;
			var words_string = "";
			if (n_length <= 9) {
				var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
				var received_n_array = new Array();
				for (var i = 0; i < n_length; i++) {
					received_n_array[i] = number.substr(i, 1);
				}
				for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
					n_array[i] = received_n_array[j];
				}
				for (var i = 0, j = 1; i < 9; i++, j++) {
					if (i == 0 || i == 2 || i == 4 || i == 7) {
						if (n_array[i] == 1) {
							n_array[j] = 10 + parseInt(n_array[j]);
							n_array[i] = 0;
						}
					}
				}
				value = "";
				for (var i = 0; i < 9; i++) {
					if (i == 0 || i == 2 || i == 4 || i == 7) {
						value = n_array[i] * 10;
					} else {
						value = n_array[i];
					}
					if (value != 0) {
						words_string += words[value] + " ";
					}
					if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
						words_string += "Crores ";
					}
					if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
						words_string += "Lakhs ";
					}
					if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
						words_string += "Thousand ";
					}
					if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
						words_string += "Hundred and ";
					} else if (i == 6 && value != 0) {
						words_string += "Hundred ";
					}
				}
				words_string = words_string.split("  ").join(" ");
			}
			return words_string;
		},
		pincode: function(value, option) {
			return value.toString().trim().substr(value.toString().length - 6);
		},
		feemap: function(value, option) {
			const FeeTypeStr = {
				0: 'Near Store Processing Fee',
				1: 'Delivery Fees',
				2: 'Coordination Fees',
				3: 'Processing Fees',
				4: 'Packaging Fees',
				5: 'Miscellaneous Fees',
				6: 'Discounts',
				7: 'Discount Code',
			};
			return FeeTypeStr[parseInt(value)];
		},
	},
}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({
	secret: 'SCMgjkoyDY/2UqZIwwV0RWq7ymPxJOhwu7YXK+jdE2A=',
	saveUninitialized: true,
	resave: true,
	// cookie: { secure: true },
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);

models.sequelize.sync().then(() => {
	app.listen(port, async () => {
		logger.info(`server running at http://localhost:${port}`);
	});
});

process.on('SIGINT', async (signal) => {
	logger.info(`got singal ${signal}, exitting gracefully....`);
	await models.sequelize.close();
	process.exit(0);
});
