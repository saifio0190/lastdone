/* eslint-disable no-multi-spaces */
const pg            = require('pg');
const Client        = pg.Pool;
let client          = null;
const log4js        = require('log4js');
const logger        = log4js.getLogger('connection/index.js');
logger.level        = log4js.levels.INFO.levelStr;

/**
 * Establishes database connection
 * @return {Object} pg client
 */
function getConnection() {
	return null;
}

module.exports = {
	getConnection,
};
