module.exports = {
    apps : [{
      name        : "my-app",
      script      : "app.js", // path needs to be relative from ecosystem.config.js
      watch       : true, // any changes to app folder will get pm2 to restart app
      instances : "max",
    exec_mode : "cluster",
    port:1133,
      env         : {
        "NODE_ENV": "production", // define env variables here
      }
    },
    {
        name        : "my-app1",
        script      : "app.js", // path needs to be relative from ecosystem.config.js
        watch       : true, // any changes to app folder will get pm2 to restart app
        port:1134,
        exec_mode : "cluster",
        env         : {
          "NODE_ENV": "development", // define env variables here
        }
      }
]
  }